import React, { useEffect, useState } from "react";
import {
  SafeAreaView,
  View,
  Text,
  ActivityIndicator,
  Image,
} from "react-native";
import Toast from "react-native-toast-message";
import inspiration from "./utilities/Inspiration";
import shuffle from "shuffle-array";
////////////////////////////3th////////////////////////////
import analytics from "@react-native-firebase/analytics";
import crashlytics from "@react-native-firebase/crashlytics";
import { useRoute } from "@react-navigation/native";
import Video from "react-native-video";
import Login from "./../components/Login";
import videoFile from "./../files/landing4.mp4";
import styles from "./HomeScreen_Styles";
import errorImage from "../design/error.png";
import landing4Png from "../files/landing4.png";
import logo from "../design/logo.png";
///////////////////////////Global/////////////////////////////
import { _rootInit } from "./utilities/Global";
const _root = _rootInit();
////////////////////////////////////////////////////////
import {
  readArticles,
  readHeader,
  readHeaderFavorites,
  readHeaderProfile,
  readHeaderContact,
  readHeaderSubscription,
  readHeaderConfig,
} from "./utilities/LoadInitialData";
// import { _DEFAULT_PROGRESS_UPDATE_INTERVAL_MILLIS } from "expo-av/build/AV";
import { BackgroundController } from "./../components/BackgroundController";

const HomeScreen = ({ route, navigation }) => {
  try {
    //const route = useRoute();
    ///////////////////
    const [loading, setLoading] = useState(true);
    const [errorLoad, setErrorLoad] = useState(false);
    ///////////////////
    async function loadInit() {
      try {
        console.log("Cargando artículos...");
        // Cargar el contenido principal
        const articles = await readArticles();
        const header = await readHeader();
        const headerFavorites = await readHeaderFavorites();
        const headerProfile = await readHeaderProfile();
        const headerContact = await readHeaderContact();
        const headerSubscription = await readHeaderSubscription();
        const headerConfig = await readHeaderConfig();

        // console.log("articles", articles);
        // console.log("header", header);
        // console.log("headerProfile", headerProfile);

        //Si algunos datos vienen vacios enviamos un error.
        if (articles.length == 0 || header.length == 0) {
          throw new Error("Server error");
        }

        _root.articles = articles;
        _root.headers = {
          header,
          headerFavorites,
          headerProfile,
          headerContact,
          headerSubscription,
          headerConfig,
        };

        console.log("HEADERS", _root.headers)

        // Al terminar la carga quitar el loader

        setLoading(false);
        setErrorLoad(false);
      } catch (e) {
        // Si existe un error en la carga ejecutar el error
        console.log(e.message);
        setErrorLoad(true);
      }
    }

    useEffect(() => {
      /////////////Quote
      shuffle(inspiration);
      Toast.show({
        type: "quote",
        text1: inspiration[0].quote,
        text2: inspiration[0].name,
        topOffset: 60,
        visibilityTime: 4000,
        autoHide: true,
      });
      /////////////Quote

      //Iniciamos la carga del contenido
      
   
      const unsubscribe = navigation.addListener("focus", () => {
        analytics().logScreenView({
          screen_class: route.name,
          screen_name: route.name,
        });
        loadInit();
        try {
          const { action } = route.params;
          if (action == "logout" || action == "suggestLogin") {
            // console.log("HOMESCREEN---> recargar!");
            setLoading(true);
            setErrorLoad(false);
            loadInit();
          }
        } catch (e) {}

        //console.log("route.params", route.params);
      });
      return unsubscribe;
    }, []);

    return (
      <BackgroundController noscroll={true} simple={true}>
        {!errorLoad && (
          <View style={styles.welcome}>
            <Video
              source={videoFile}
              style={styles.backgroundVideo}
              muted={true}
              repeat={true}
              resizeMode={"cover"}
              rate={1.0}
              poster={
                "https://storage.googleapis.com/ux-tips-9e78f.appspot.com/landing4.png"
              }
            />
            <View style={styles.loginContainer}>
              <Image source={logo} style={styles.logo}></Image>

              {/* -----------------Cargar login---------------  */}
              <View style={styles.loginButtonsContainer}>
                <Login
                  navigation={navigation}
                  hideGuestButton={false}
                  changeEntities={(data) => {
                    console.log("NEW ENTITIES: ", data);
                  }}
                  section={"home"}
                  loading={loading}
                ></Login>
              </View>
              {/* --------------Cargar login------------------  */}
            </View>
          </View>
        )}
        {errorLoad && (
          <View style={styles.errorContainer}>
            <Image source={errorImage} style={styles.errorImage}></Image>
            <Text style={styles.errorText2}>
              ¿Todo bien con tu conexión a internet?
            </Text>
            <Text style={styles.errorText}>
              Ocurrió un problema al cargar el contenido, quizá pueden ser
              nuestros servidores, no estamos seguros...
            </Text>
          </View>
        )}
      </BackgroundController>
    );
  } catch (e) {
    // crashlytics().crash();
  }
};

export default HomeScreen;
