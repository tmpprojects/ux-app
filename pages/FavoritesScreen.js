import React, { useState, useEffect, useRef } from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";
import Toast from "react-native-toast-message";

import {
  Modal,
  ModalPortal,
  SlideAnimation,
  ModalContent,
  ModalTitle,
  ModalFooter,
  ModalButton,
} from "react-native-modals";
import {
  View,
  Text,
  SectionList,
  Image,
  Pressable,
  Platform,
  Button,
  Animated,
  Easing,
  Alert,
  ScrollView,
} from "react-native";
////////////////////////////3th////////////////////////////
import analytics from "@react-native-firebase/analytics";
import crashlytics from "@react-native-firebase/crashlytics";
import { useRoute } from "@react-navigation/native";
import Icon2 from "react-native-vector-icons/Feather";
import Icon3 from "react-native-vector-icons/FontAwesome5";
import Item from "../components/Item";
import Modalito from "../components/Modalito";
import {
  saveFavorite,
  getFavorites,
  deleteFavorite,
  viewIconFav,
} from "./utilities/SaveFavorite";

import { LoopAnim,
  ScaleAnim,
  EnterAnim,
  RotateAnim,
 } from "./utilities/Animations";
import { Grayscale } from "react-native-color-matrix-image-filters";

import moment from "moment";
import "moment-timezone";
moment().tz("America/Mexico_City").format();
//https://storage.googleapis.com/ux-tips-9e78f.appspot.com/post-1_1.png

import { _rootInit } from "./utilities/Global";
const _root = _rootInit();

import styles from "./DetailScreen_Styles";
import stylesInteraction from "./utilities/Animation_Styles";
import lock from "./../design/lock.png";
import unlock from "./../design/unlock.png";
import members from "./../design/members.png";
import membersrRegister from "./../design/membersrRegister.png";

import { BackgroundController } from "./../components/BackgroundController";

const FavoritesScreen = ({ navigation }) => {
  try {
    //
    const scrollRef = useRef();
    const route = useRoute();
    const [header, setHeader] = useState(
      _root.headers.header.src + "?cache=" + Math.random()
    );
    const [data, setData] = useState([]);
    const [modalVisible, setModalVisible] = useState(false);
    const [enterRun, setEnterRun] = useState(false);
    const [member, setMember] = useState(_root.entities.premium);
    const [isScrollEnabled, setIsScrollEnabled] = useState(true);
    const [emptyFavs, setEmptyFavs] = useState(false);

    useEffect(() => {
      const unsubscribe = navigation.addListener("focus", () => {
        syncFavorites();
        viewIconFav("true");
        analytics().logScreenView({
          screen_class: route.name,
          screen_name: route.name,
        });
        setEnterRun(true);
        setMember(_root.entities.premium);
        // console.log("wrapperOptions route.name ", route.name);
      });
      const unsubscribeBlur = navigation.addListener("blur", () => {
        setEnterRun("off");
      });

      return unsubscribe, unsubscribeBlur;
    }, []);

    function prepareRender() {
      // console.log(_root.recoveryFavorites.length);
      if (_root.recoveryFavorites.length) {
        const newData = _root.recoveryFavorites.sort(
          (a, b) => b.position - a.position
        );

        console.log(newData);
        // console.log("newData", newData);
        setData(newData);
        setEmptyFavs(false);
      } else {
        // console.log("empty!");
        setEmptyFavs(true);
        setData([]);
      }
    }

    async function syncFavorites() {
      let getAllFavorites = await getFavorites();
      _root.recoveryFavorites = getAllFavorites;
      prepareRender();
    }

    // console.log("data--->", data)
    return (
      // <SafeAreaView style={styles.container}>

      <BackgroundController noscroll={true}>
        <Modalito
          modalVisible={modalVisible}
          setModalVisible={setModalVisible}
          navigation={navigation}
        ></Modalito>
        {/* <EnterAnim run={enterRun}> */}
        <View style={styles.header}>
          <View style={styles.cover}>
            <Image
              style={styles.coverImage}
              source={{
                uri: header,
              }}
            />
            <View style={stylesInteraction.interactiveLeft}>
              <RotateAnim
                run={enterRun}
                appear={12}
                image={0}
                speed={6}
                reverse={false}
              />
            </View>

            <View style={stylesInteraction.interactiveRight2}>
              <RotateAnim
                run={enterRun}
                appear={4}
                image={5}
                speed={2}
                reverse={true}
              />
            </View>
            <View style={stylesInteraction.interactiveLeft2}>
              <RotateAnim
                run={enterRun}
                appear={8}
                image={5}
                speed={3}
                reverse={false}
              />
            </View>

            <View style={stylesInteraction.interactiveRight3}>
              <RotateAnim
                run={enterRun}
                appear={8}
                image={3}
                speed={8}
                reverse={true}
              />
            </View>
          </View>
        </View>
        {/* </EnterAnim> */}
        {data.length > 0 && (
          <ScrollView
            style={styles.sectionList}
            ref={scrollRef}
            scrollEnabled={isScrollEnabled}
          >
            {data.map((item, index) => {
              return (
                <Item
                  content={item}
                  navigation={navigation}
                  setModalVisible={setModalVisible}
                  member={member}
                  disabledFavorite={true}
                  goIntern={"FullDetailFavScreen"}
                  key={index}
                  syncFavorites={syncFavorites}
                />
              );
            })}
          </ScrollView>
        )}
        {emptyFavs && (
          <View style={styles.emptyFavs}>
            <View>
              <LoopAnim run={true}>
                <Icon3
                  name="heart-broken"
                  size={40}
                  style={styles.broken}
                ></Icon3>
              </LoopAnim>
            </View>

            <Text style={styles.brokenText}>Aún no hay favoritos.</Text>
          </View>
        )}
      </BackgroundController>
    );
  } catch (e) {
    console.log(e.message);
    return null;
    //crashlytics().crash();
  }
};

export default FavoritesScreen;
