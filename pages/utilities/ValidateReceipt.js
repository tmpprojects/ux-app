import React, { useEffect, useState } from "react";
import {
  SafeAreaView,
  View,
  Text,
  ActivityIndicator,
  Image,
  Linking,
  Alert,
  Platform,
  Pressable,
} from "react-native";
import config from "./../../env";

import firestore from "@react-native-firebase/firestore";
import {
  setAsyncSubscription,
  getAsyncSubscription,
  setAsyncExpired,
} from "../utilities/SaveSubscription.js";

////////////////////////////3th////////////////////////////
import analytics from "@react-native-firebase/analytics";
import crashlytics from "@react-native-firebase/crashlytics";
import { useRoute } from "@react-navigation/native";
import axios from "react-native-axios";
import { _rootInit } from "../utilities/Global";
const _root = _rootInit();
import * as RNIap from "react-native-iap";

const productIds = Platform.select({
  ios: ["mx.com.uxapp.member"],
  android: [""],
});
const UXMemberProduct = "mx.com.uxapp.member";

function validateRemoteReceipt(uid) {
  try {
    // !!!!! uid es en realidad el documento, no uid.
    console.log(" $$$$$ Validar recibo", uid);
    if (Boolean(uid)) {
      return axios
        .post(config().syncCloudFunction, { uid })
        .then((result) => {
          let isExpired = result.data.expired;
          if (isExpired) {
            //Esta expirado
            _root.entities.premium = false;
            _root.entities.subscriptionType = "expired";
            setAsyncExpired();
            const updateDoc = firestore().collection("members").doc(uid);
            updateDoc.update({
              subscriptionType: "expired",
            });
            console.log(" $$$$$ Suscripción expirada ", uid);
          } else {
            _root.entities.premium = true;
            _root.entities.subscriptionType = "active";
            console.log(" $$$$$ Suscripción activa ", uid);
          }
          return result;
        })
        .catch((e) => {
          crashlytics().recordError(e);
          console.log("F I R E B A S E - ERROR 🧨 (sin recivo)", e.message);
          return false;
        });
    }
  } catch (e) {
    crashlytics().recordError(e);
  }
}

export { validateRemoteReceipt };
