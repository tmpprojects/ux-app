import { _rootInit } from "./Global";
import AsyncStorage from "@react-native-async-storage/async-storage";
const _root = _rootInit();

export async function saveFavorite(item) {
  //console.log( item.id );
  if (item) {
    AsyncStorage.setItem("favsView", "false");

    const increase = await AsyncStorage.getItem("increaseFavorite");
    const increaseOrder = await AsyncStorage.getItem("increaseOrderFavorite");
    const newFav = Number(increase) + 1;
    const newFavOrder = Number(increaseOrder) + 1;
    AsyncStorage.setItem("increaseFavorite", `${newFav}`);
    AsyncStorage.setItem("increaseOrderFavorite", `${newFavOrder}`);

    let newPosition = { ...item, position: newFavOrder };
    _root.favorites[item.id] = newPosition;

    for (let i in _root.favorites) {
      // console.log(i);
    }
    let saveJson = JSON.stringify({ ..._root.favorites });
    AsyncStorage.setItem("favorites", saveJson);
    //////////////
  }
}
export async function getFavorites() {
  //console.log( item.id );
  const getAllFavorites = await AsyncStorage.getItem("favorites");
  if (!getAllFavorites) {
    return [];
  }
  let getResult = Object.values(JSON.parse(getAllFavorites));
  return getResult;
}

export async function getNumFavorites() {
  //console.log( item.id );
  const getNumFavorites = await AsyncStorage.getItem("increaseFavorite");
  const getNumOrderFavorites = await AsyncStorage.getItem(
    "increaseOrderFavorite"
  );
  if (!getNumFavorites) {
    return 0;
  }
  return getNumFavorites;
}
export async function deleteFavorite(id) {
  // console.log("borrar---->", id);
  // console.log("antes--->", _root.favorites);
  delete _root.favorites[id];
  // console.log("despues--->", _root.favorites);
  let saveJson = JSON.stringify({ ..._root.favorites });
  await AsyncStorage.setItem("favorites", saveJson);

  // const deleteId = _root.recoveryFavorites.filter((item) => item.id != id);
  // _root.recoveryFavorites = deleteId;
  // _root.favorites = deleteId;
}

export async function viewIconFav(action = "read") {
  if (action == "read") {
    const statusView = await AsyncStorage.getItem("favsView");
    return statusView;
  } else {
    if (action == "true" || action == true) {
      await AsyncStorage.setItem("increaseFavorite", "0");
    }

    await AsyncStorage.setItem("favsView", `${action}`);
  }
}

export async function initFavorites() {
  try {
    const increase = await AsyncStorage.getItem("increaseFavorite");
    const increaseOrder = await AsyncStorage.getItem("increaseOrderFavorite");
    if (!increase) {
      await AsyncStorage.setItem("increaseFavorite", "0");
      await AsyncStorage.setItem("increaseOrderFavorite", "0");
    }

    //console.log( item.id );
    const getAllFavorites = await AsyncStorage.getItem("favorites");
    if (!getAllFavorites) {
      return [];
    }
    const loadFavorites = Object.values(JSON.parse(getAllFavorites));
    for (let item of loadFavorites) {
      _root.favorites[item.id] = item;
    }
  } catch (e) {
    console.log(e.message);
  }
}
