import React, { useState, useEffect, useRef } from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";
import styles from "./Animation_Styles";
import {
  Modal,
  ModalPortal,
  SlideAnimation,
  ModalContent,
  ModalTitle,
  ModalFooter,
  ModalButton,
} from "react-native-modals";
import {
  View,
  Text,
  SectionList,
  Image,
  Pressable,
  Platform,
  Button,
  Animated,
  Easing,
} from "react-native";
import interactive0 from "./../../design/interactive-0.png";
import interactive1 from "./../../design/interactive-1.png";
import interactive2 from "./../../design/interactive-2.png";
import interactive3 from "./../../design/interactive-3.png";
import interactive4 from "./../../design/interactive-4.png";
import interactive5 from "./../../design/interactive-5.png";

export function LoopAnim({ children, run }) {
  useEffect(() => {
    if (run) {
      if (run == "off") {
        offAnimation();
      } else {
        runAnimation();
      }
    }
  }, [run]);
  const startLoopScale = useRef(new Animated.Value(1)).current;
  const runAnimation = () => {
    Animated.loop(
      Animated.sequence([
        Animated.timing(startLoopScale, {
          toValue: 1.5,
          duration: 100,
          useNativeDriver: true,
        }),
        Animated.timing(startLoopScale, {
          toValue: 1,
          duration: 700,
          useNativeDriver: true,
        }),
      ]),
      { iterations: 1000 }
    ).start();
  };

  return (
    <Animated.View
      style={[
        styles.square,
        {
          transform: [
            {
              scale: startLoopScale,
            },
          ],
        },
      ]}
    >
      {children}
    </Animated.View>
  );
}
export function ScaleAnim({ children, run }) {
  useEffect(() => {
    if (run) {
      if (run == "off") {
        offAnimation();
      } else {
        runAnimation();
      }
    }
  }, [run]);

  const scaleFav = useRef(new Animated.Value(1)).current;
  const runAnimation = () => {
    // console.log("escalar");
    // Will change fadeAnim value to 1 in 5 seconds
    Animated.sequence([
      Animated.timing(scaleFav, {
        toValue: 1.9,
        duration: 70,
        useNativeDriver: true,
      }),
      Animated.timing(scaleFav, {
        toValue: 0.4,
        duration: 80,
        useNativeDriver: true,
      }),
      Animated.timing(scaleFav, {
        toValue: 1.6,
        duration: 100,
        useNativeDriver: true,
      }),
      Animated.timing(scaleFav, {
        toValue: 0.8,
        duration: 150,
        useNativeDriver: true,
      }),
      Animated.timing(scaleFav, {
        toValue: 1.2,
        duration: 200,
        useNativeDriver: true,
      }),
      Animated.timing(scaleFav, {
        toValue: 1,
        duration: 300,
        useNativeDriver: true,
      }),
    ]).start();
  };

  const offAnimation = () => {
    // console.log("escalar");
    // Will change fadeAnim value to 1 in 5 seconds
    Animated.sequence([
      Animated.timing(scaleFav, {
        toValue: 1.6,
        duration: 100,
        useNativeDriver: true,
      }),

      Animated.timing(scaleFav, {
        toValue: 1,
        duration: 200,
        useNativeDriver: true,
      }),
    ]).start();
  };

  return (
    <Animated.View
      style={[
        styles.fadingContainer,
        {
          transform: [{ scale: scaleFav }],
        },
      ]}
    >
      {children}
    </Animated.View>
  );
}

////////////////////////////////////////////////////////////////
export function EnterAnim({ children, run }) {
  const initTranslateY = 50;
  const initScale = 1.2;
  const initOpacity = 0;
  const translateY = useRef(new Animated.Value(initTranslateY)).current;
  const dynOpacity = useRef(new Animated.Value(initOpacity)).current;
  const dynScale = useRef(new Animated.Value(initScale)).current;
  useEffect(() => {
    if (run) {
      if (run == "off") {
        offAnimation();
      } else {
        runAnimation();
      }
    }
  }, [run]);
  const runAnimation = () => {
    // console.log("escalar");
    // Will change fadeAnim value to 1 in 5 seconds
    Animated.timing(translateY, {
      toValue: 0,
      duration: 400,
      //easing: Easing.in(Easing.bezier(1, 0.26, 0.1, 1.4)),
      useNativeDriver: true,
    }).start();
    Animated.timing(dynOpacity, {
      toValue: 1,
      duration: 300,
      useNativeDriver: true,
    }).start();
    Animated.timing(dynScale, {
      toValue: 1,
      duration: 300,
      useNativeDriver: true,
    }).start();
  };

  const offAnimation = () => {
    // console.log("mover fuera");
    Animated.timing(translateY, {
      toValue: initTranslateY,
      duration: 0,
      useNativeDriver: true,
    }).start();
    Animated.timing(dynOpacity, {
      toValue: initOpacity,
      duration: 500,
      useNativeDriver: true,
    }).start();
    Animated.timing(dynScale, {
      toValue: initScale,
      duration: 300,
      useNativeDriver: true,
    }).start();
  };

  return (
    <Animated.View
      style={[
        styles.fadingContainer,
        {
          transform: [{ translateY: translateY }, { scale: dynScale }],
          opacity: dynOpacity,
        },
      ]}
    >
      {children}
    </Animated.View>
  );
}

///////////////

export function RotateAnim({ image, speed = 8, reverse = false, run = true, appear=8, appearReverse=false}) {
  const initPositionY = appearReverse ? -100 : 100;
  const dynRotate = new Animated.Value(0);
  const dynOpacity = useRef(new Animated.Value(0)).current;
  const dynPositionY = useRef(new Animated.Value(initPositionY)).current;

  const spin = dynRotate.interpolate({
    inputRange: [0, 1],
    outputRange: ["0deg", reverse ? "-360deg" : "360deg"],
  });

  useEffect(() => {
    if (run) {
      if (run == "off") {
        offAnimation();
      } else {
        runAnimation();
      }
    }
  }, [run]);
  const runAnimation = () => {

    Animated.timing(dynOpacity, {
      toValue: 1,
      duration: appear*100,
      useNativeDriver: true,
    }).start();
    Animated.timing(dynPositionY, {
      toValue: 0,
      duration: appear*400,
      useNativeDriver: true,
      easing: Easing.in(Easing.bezier(.06,.56,.37,.99))
    }).start();
  };
  const offAnimation = () => {

    Animated.timing(dynOpacity, {
      toValue: 0,
      duration: 0,
      useNativeDriver: true,
    }).start();

    Animated.timing(dynPositionY, {
      toValue: initPositionY,
      duration: 0,
      useNativeDriver: true,
     
    }).start();
  };

  // Animated.timing(dynRotate, {
  //   toValue: 1,
  //   duration: 3000,
  //   easing: Easing.linear, // Easing is an additional import from react-native
  //   useNativeDriver: true, // To make use of native driver for performance
  // }).start();

  Animated.loop(
    Animated.timing(dynRotate, {
      toValue: 1,
      duration: speed * 1000,
      easing: Easing.linear,
      useNativeDriver: true,
    })
  ).start();
  let selectImage;
  switch (image) {
    case 0:
      selectImage = interactive0;
      break;
    case 1:
      selectImage = interactive1;
      break;
    case 2:
      selectImage = interactive2;
      break;
    case 3:
      selectImage = interactive3;
      break;
    case 4:
      selectImage = interactive4;
      break;
    case 5:
      selectImage = interactive4;
      break;
    default:
      selectImage = interactive0;
      break;
  }
  return (
    <Animated.Image
      style={[
        styles.interactiveImage,
        {
          transform: [{ rotate: spin }, {translateY: dynPositionY}],
          opacity: dynOpacity,
        },
      ]}
      source={selectImage}
    />
  );
}
