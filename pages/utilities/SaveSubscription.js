import { _rootInit } from "./Global";
import AsyncStorage from "@react-native-async-storage/async-storage";
const _root = _rootInit();

export async function setAsyncExpired() {
  AsyncStorage.setItem("subscription", `expired`);
}

export async function setAsyncSubscription(mode = "none", receipt = "") {
  AsyncStorage.setItem("subscription", `${mode}`);
  AsyncStorage.setItem("receipt", `${receipt}`);
}
export async function getAsyncSubscription() {
  //console.log( item.id );
  let getSubscription = await AsyncStorage.getItem("subscription");
  let getReceipt = await AsyncStorage.getItem("receipt");
  if (!getSubscription) {
    getSubscription = "none";
  }
  if (!getReceipt) {
    getReceipt = "";
  }

  return {
    subscription: getSubscription,
    receipt: getReceipt,
  };
}
