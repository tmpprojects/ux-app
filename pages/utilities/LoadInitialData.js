import React, { useEffect, useState } from "react";
////////////////////////////3th////////////////////////////
import firestore from "@react-native-firebase/firestore";
import moment from "moment";
///////////////////////////Global/////////////////////////////
import { _rootInit } from "../utilities/Global";
const _root = _rootInit();
////////////////////////////////////////////////////////

export async function readArticles() {
  return new Promise(async (resolve, reject) => {
    let articles = [];
    await firestore()
      .collection("articles")
      .orderBy("date", "desc")
      .get()
      .then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          articles.push({
            id: doc.id,
            data: doc.data(),
            date: moment(doc.data().date).format("L"),
          });
        });
      });
    resolve(articles);

  });
}

export async function readHeader() {
  return new Promise(async (resolve, reject) => {
    let header = [];
    await firestore()
      .collection("headers")
      .doc("header")
      .get()
      .then((doc) => {
        header = doc.data();
      });
    resolve(header);
  });
}
export async function readHeaderProfile() {
  return new Promise(async (resolve, reject) => {
    let header = [];
    await firestore()
      .collection("headers")
      .doc("headerProfile")
      .get()
      .then((doc) => {
        header = doc.data();
      });
    resolve(header);
  });
}

export async function readHeaderFavorites() {
  return new Promise(async (resolve, reject) => {
    let header = [];
    await firestore()
      .collection("headers")
      .doc("headerFavorites")
      .get()
      .then((doc) => {
        header = doc.data();
      });
    resolve(header);
  });
}

export async function readHeaderContact() {
  return new Promise(async (resolve, reject) => {
    let header = [];
    await firestore()
      .collection("headers")
      .doc("headerContact")
      .get()
      .then((doc) => {
        header = doc.data();
      });
    resolve(header);
  });
}

export async function readHeaderSubscription() {
  return new Promise(async (resolve, reject) => {
    let header = [];
    await firestore()
      .collection("headers")
      .doc("headerSubscription")
      .get()
      .then((doc) => {
        header = doc.data();
      });
    resolve(header);
  });
}

export async function readHeaderConfig() {
  return new Promise(async (resolve, reject) => {
    let header = [];
    await firestore()
      .collection("headers")
      .doc("headerConfig")
      .get()
      .then((doc) => {
        header = doc.data();
      });
    resolve(header);
  });
}
