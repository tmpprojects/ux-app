//import { Subject } from "rxjs";
import { BehaviorSubject } from "rxjs/BehaviorSubject";

let globalThis = global;

const subject = new BehaviorSubject();

let _root_initials_to_reset = () => {
  console.log("[[[[[[[[[[[[[RESET]]]]]]]]]]]]");
  const reset = {
    // config: {},
    // articles: {},
    // favorites: [],
    // recoveryFavorites: [],
    user: {
      name: "",
      email: "",
      aliasEmail: "",
      uniqId: "",
      aliasName: "",
      receipt: "",
      purchaseId: "",
      providerId: "",
      infoDevice: {},
      authId: "",
      date:"",
      subscriptionType:"",
      id:"",
      check_remote_subscription:"",
      experience: "principiante",
      instagram: "",
      twitter: "",
      linkedin: "",
      website: "",
      enabledRRSS: false,
      expired: false,
    },
    // headers: {},
    selected: {},
    // watchUserStatus: new Subject(),
    // watchFavs: new Subject(),
    entities: {
      memberType: "Guest", //LoggedIn, Guest
      subscriptionType: "none", // none, expired, active
    },
  };
  globalThis.UxApp.user = {
    ...globalThis.UxApp.user,
    ...reset.user,
  };
  globalThis.UxApp.selected = {
    ...globalThis.UxApp.selected,
    ...reset.selected,
  };
  globalThis.UxApp.entities = {
    ...globalThis.UxApp.entities,
    ...reset.entities,
  };
};

let _root_initials = () => {
  globalThis.UxApp = {
    config: {},
    articles: {},
    favorites: [],
    recoveryFavorites: [],
    user: {
      name: "",
      email: "",
      aliasEmail: "",
      uniqId: "",
      aliasName: "",
      receipt: "",
      purchaseId: "",
      providerId: "",
      infoDevice: {},
      authId: "",
      date:"",
      subscriptionType:"",
      id:"",
      check_remote_subscription:"",
      experience: "principiante",
      instagram: "",
      twitter: "",
      linkedin: "",
      website: "",
      enabledRRSS: false,
      expired: false,
    },
    headers: {},
    selected: {},
    watchFavs: {
      send: (message) => subject.next(message),
      clear: () => subject.next(),
      get: () => subject,
    },
    entities: {
      memberType: "Guest", //LoggedIn, Guest
      subscriptionType: "none", // none, expired, active
    },
  };
};

let _rootInit = () => {
  if (!Boolean(globalThis.UxApp)) {
    console.log("[[[[[[[[[[[[[[CREATE ROOT]]]]]]]]]]]]]]]]]]]]]");
    _root_initials();
    return globalThis.UxApp;
  } else {
    return globalThis.UxApp;
  }
};
export { _rootInit, _root_initials, _root_initials_to_reset };
