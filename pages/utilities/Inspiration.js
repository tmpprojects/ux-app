 const inspiration = [
  {
    quote: "El único modo de hacer un gran trabajo es amar lo que haces",
    name: "Steve Jobs",
  },
  {
    quote:
      "El dinero no es la clave del éxito; la libertad para poder crear lo es",
    name: "Nelson Mandela",
  },
  {
    quote:
      "El trabajo duro hace que desaparezcan las arrugas de la mente y el espíritu",
    name: "Helena Rubinstein",
  },
  {
    quote:
      "Cuando algo es lo suficientemente importante, lo haces incluso si las probabilidades de que salga bien no te acompañan",
    name: "Elon Musk",
  },
  {
    quote: "A veces la adversidad es lo que necesitas encarar para ser exitoso",
    name: "Zig Ziglar",
  },
  {
    quote:
      "Ejecuta tus conocimientos con la maestría del que sigue aprendiendo",
    name: "Jonathan García-Allen",
  },
  { quote: "Cuando pierdas, no pierdas la lección", name: "Dalai Lama" },
  {
    quote: "Tienes que hacer las cosas que crees que no puedes hacer",
    name: "Eleanor Roosevelt",
  },
  {
    quote: "Si la oportunidad no llama, construye una puerta",
    name: "Milton Berle",
  },
  { quote: "El valor de una idea radica en su uso", name: "Thomas Edison" },
  {
    quote: "Con autodisciplina casi todo es posible",
    name: "Theodore Roosevelt",
  },
  {
    quote:
      "Asegúrate de que colocas tus pies en el lugar correcto, y luego mantente firme",
    name: "Abraham Lincoln",
  },
  {
    quote: "Tienes que esperar cosas de ti mismo antes de poder hacerlas",
    name: "Michael Jordan",
  },
  { quote: "Transforma tus heridas en sabiduría", name: "Oprah Winfrey" },
  {
    quote: "La edad no es barrera. Es una limitación que pones en tu mente",
    name: "Jackie Joyner-Kersee",
  },
  {
    quote: "La innovación distingue al líder del seguidor",
    name: "Steve Jobs",
  },
  { quote: "Si puedes soñarlo, puedes hacerlo ", name: "Walt Disney" },
  { quote: "El secreto para salir adelante es comenzar", name: "Mark Twain" },
  {
    quote:
      "Los obstáculos son esas cosas atemorizantes que ves cuando apartas los ojos de tu meta",
    name: "Henry Ford",
  },
  {
    quote:
      "La vida es como montar en bicicleta. para mantener el equilibrio tienes que avanzar",
    name: "Albert Einstein",
  },
  {
    quote:
      "El futuro pertenece a aquellos que creen en la belleza de sus sueños",
    name: "Eleanor Roosevelt",
  },
  {
    quote: "No pares cuando estés cansado. Para cuando hayas terminado",
    name: "Marilyn Monroe",
  },
  {
    quote: "Cree que puedes y casi lo habrás logrado",
    name: "Theodore Roosevelt",
  },
  {
    quote:
      "Si tienes un sueño y crees en él, corres el riesgo de que se convierta en realidad",
    name: "Walt Disney",
  },
  {
    quote:
      "El éxito es la suma de pequeños esfuerzos, que se repiten día tras día",
    name: "Robert Collier",
  },
  { quote: "Entrega siempre más de lo que esperan de ti", name: "Google" },
  {
    quote: "Nunca es demasiado tarde para ser lo que podrías haber sido",
    name: "George Eliot",
  },
  {
    quote: "La mejor forma de predecir el futuro es creándolo",
    name: "Abraham Lincoln ",
  },
  {
    quote: "Hoy eres un lector y mañana serás un líder",
    name: "Margaret Fuller",
  },
  {
    quote:
      "Se lo suficientemente valiente para vivir de forma creativa. El lugar creativo donde nadie ha estado",
    name: "Alan Alda",
  },
  {
    quote: "Cuando dejo ir lo que soy, me convierto en lo que debería ser",
    name: "Cuando dejo ir lo que soy, me convierto en lo que debería ser",
  },
  {
    quote: "Apunta a la luna. Si fallas, podrías dar a una estrella",
    name: "William Clement Stone",
  },
  {
    quote:
      "La forma más rápida de cambiar es convivir con personas que ya son como quieres ser",
    name: "Reid Hoffman",
  },
  {
    quote:
      "La verdadera motivación procede de trabajar en cosas que nos importan",
    name: "Sheryl Sandberg",
  },
  { quote: "La buena vida es una forma de pensar", name: "Emilio Valcárcel" },
  {
    quote: "Puedes ser feliz allí donde estés",
    name: "Puedes ser feliz allí donde estés",
  },
  {
    quote:
      "Alguien se sienta en la sombra porque alguien plantó un árbol hace mucho tiempo",
    name:
      "Alguien se sienta en la sombra porque alguien plantó un árbol hace mucho tiempo",
  },
  {
    quote:
      "Alguien se sienta en la sombra porque alguien plantó un árbol hace mucho tiempo",
    name: "Thoreau",
  },
  {
    quote: "Sólo se vive una vez. Pero si lo haces bien, una vez basta",
    name: "Mae West",
  },
  {
    quote:
      "Da el primer paso con fe. No tienes que ver todas las escaleras, sólo da el  primer paso",
    name: "Martin Luther King",
  },
  {
    quote:
      "Trabajar duro por algo que no te importa se llama estrés. Trabajar duro por algo que te importa de verdad se llama pasión",
    name: "Simon Sinek",
  },
  {
    quote: "Intenta y falla, pero nunca falles en intentarlo",
    name: "Jared Leto",
  },
  {
    quote:
      "Por más difícil que parezca la vida, siempre hay algo que puedes hacer y en lo que puedes tener éxito",
    name: "Stephen Hawking",
  },
  {
    quote:
      "La mala noticia es que el tiempo vuela. La buena noticia es que tú eres el piloto",
    name: "Michael Altshuler",
  },
  {
    quote:
      "La mejor forma de olvidar las malas cosas de la vida es aprender a recordar las cosas buenas",
    name: "Mark Amend",
  },
  {
    quote:
      "El éxito es la capacidad de ir de fracaso en fracaso sin perder el entusiasmo ",
    name: "Winston Churchill",
  },
  {
    quote:
      "Tus acciones positivas combinadas con los pensamientos positivos generan éxitos",
    name: "Shiv Khera",
  },
  {
    quote:
      "Qué maravilloso es que nadie tenga que esperar ni un segundo para empezar a mejorar el mundo",
    name: "Ana Frank",
  },
  {
    quote:
      "Cualquier cosa que la mente del hombre puede concebir y creer, puede ser conseguida",
    name: "Napoleon Hill",
  },
  {
    quote:
      "La palabra «no» solo significa que empieces otra vez en un nivel superior",
    name: "Peter Diamandis",
  },
  {
    quote: "La grandeza nace de pequeños comienzos ",
    name: "Sir Francis Drake",
  },
  {
    quote: "Pon tu corazón, mente y alma incluso en los actos más pequeños.",
    name: "Swami Sivananda",
  },
];

export default inspiration;