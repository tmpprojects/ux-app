import React, { useState, useEffect } from "react";
import {
  SafeAreaView,
  View,
  FlatList,
  StyleSheet,
  Text,
  StatusBar,
} from "react-native";
export default styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  coverImage: {
    width: "100%",
    height: 300,
  },
  itemContainer: {
    display: "flex",
    flexDirection: "row",
    marginBottom: 15,
  },
 coverImage: {
    width: "100%",
    height: 300,
  },
  //////////////////////////////////
  item: {
    backgroundColor: "#f9c2ff",
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  textTitle:{
    fontSize: 30,
    marginBottom: 40,
  },
  description: {
    fontSize: 18,
    color: "#555555",
  },
  moreDescription: {
    fontSize: 14,
    color: "#A6A6A6",
  },
  textContainer: {
    padding: 15,
  },
  textDescription: {
    fontSize: 22,
    color: "#505050",
  },
  sectionList: {
    margin: 15,
    flex: 1
  },
  itemLockMessage:{
    width: '100%',
    display: 'flex',
    flexDirection:'row',
    backgroundColor: 'rgba(0,0,0,.6)',
    position: "absolute",
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    bottom: 0,
    left: 0,
    paddingTop: 8,
    paddingBottom: 8,
  
  },
  itemLockMessageText:{
    color: 'white',
    marginLeft: 10,
    fontSize:12,
  },
  itemLockImage: {
    width: 28,
    height: 28,
    borderRadius: 10,
  },
  headerSectionList: {},
  headerSectionListText: {
    fontSize: 20,
    padding: 3,
    textAlign: "right",
    fontSize: 16,
    color: "#A6A6A6",
  },
  carousel:{
    backgroundColor: 'white',
  },

  itemContainer: {
    display: "flex",
    flexDirection: "row",
    marginBottom: 15,
  },
  itemImage: {
    width: 130,
    height: 130,
  },
  mode: {
    display: "flex",
    alignItems: "flex-end",
    padding: 15,
  },
  itemInfo: {
    flex: 1,
    marginLeft: 15,
  },
  imageTinder: {
    width: "100%",
    height: 300,
  },
 
  byContainer:{
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    marginTop: 30,
    marginBottom: 60,
  },
  by:{
    width:100,
    height:100,
    resizeMode: "contain",
    borderRadius: 200,
    marginLeft: 16
  },
  byImage:{
    width: 100,
  },
  byInfo:{
    flex: 1,
    marginLeft: 40,
    paddingRight: 16,
  },
  byContactQuestion:{
    marginBottom: 12,
    color: 'rgba(0,0,0,0.6)',
  },
  byContact:{
    marginTop: 6,
    color: 'rgba(0,0,0,0.6)',
  },
  byInstagram:{
    color: '#1DBCC7',
    fontWeight: 'bold'
  },
  byEmail:{

    color: '#1DBCC7',
    fontWeight: 'bold'
  },
  separatorLine: {
    borderBottomColor: "rgba(0,0,0,.1)",
    borderBottomWidth: 1,
    marginBottom: 10,
    marginTop: 60,
  },
  exclusiveContentContainer:{
    display: 'flex',
    //alignItems: 'flex-end'
  },
  exclusiveContent:{
    width:150,
    height:50,
    resizeMode: "contain",
    marginRight: 10,
  },

  fullDetailTitleContainer:{
  flex: 1,
},
  fullDetailTitle:{
    color: '#1DBCC7',
    fontSize: 30,
    marginBottom: 30,
  },
  fullDetailTitlePremium:{
    color: '#EF9A00',
    fontSize: 30,
    marginBottom: 30,
  },
  headerBackground: {
    flex: 1,
    resizeMode: "stretch",
    justifyContent: "center",
    paddingTop:30,
    paddingBottom:30
  },
  ////////////////////////////////////////////
  bold: {
    fontWeight: "bold",
  },
  itemFavorite: {},
  unFavorite: {
    color: "white",
  },
  applyFavorite: {
    color: "#02FFD5",
    shadowColor: "#02FFD5",
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.39,
    shadowRadius: 8.3,
    //elevation: 13,
  },
  pressableItemFavorite: {
    position: "absolute",
    width: 42,
    height: 42,
    top: 0,
    right: 0,
    borderRadius: 10,
    //backgroundColor: "red",
    display: "flex",
    alignItems: "center",
    alignContent: "center",
    justifyContent: "center",
    margin:0,
  },
});
