import React, { useState, useEffect } from "react";
import {
  SafeAreaView,
  View,
  FlatList,
  StyleSheet,
  Text,
  StatusBar,
  SectionList,
  Switch,
  Image,
  Pressable,
  Platform,
} from "react-native";
export default styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },

  itemContainer: {
    display: "flex",
    flexDirection: "row",
    marginBottom: 15,
  },
 coverImage: {
    width: "100%",
    height: 410,
  },
  formLabel: {
    fontSize: 18,
    color: "#809EFF",
    marginBottom: 12,
  },
  formLabelTitle: {
    fontSize: 18,
    color: "rgba(0, 0, 0,0.6)",

  },
  infoContact: {
    fontSize: 18,
    color: "rgba(0,0,0,.6)",
    marginBottom: 18,
  },
  bold: {
    fontWeight: "bold",
  },
  formField: {
    marginBottom: 22,
  },
  terms: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
  },
  termsText: {
    fontSize: 14,
    marginTop: 20,
    color: "rgba(0,0,0,0.5)",
  },
  activityIndicator: {
    marginTop: 20,
  },
  formButtonSave: {
    borderRadius: 18,
    width: "100%",
    height: 60,
    color: "white",
    backgroundColor: "#809EFF",
    //display: 'flex',

  },
  contactContainer:{
    padding: 30,

    display: "flex",
  },
  titleSpace:{
    marginBottom: 16,
    color: 'red'
  }
});
