import React, { useEffect, useState } from "react";
import {
  SafeAreaView,
  View,
  Text,
  ActivityIndicator,
  Image,
  Linking,
  Alert,
} from "react-native";
import {
  Modal,
  ModalPortal,
  SlideAnimation,
  ModalContent,
  ModalTitle,
  ModalFooter,
  ModalButton,
} from "react-native-modals";
import Toast from "react-native-toast-message";
import members from "./../design/members.png";
import membersrRegister from "./../design/membersrRegister.png";
import auth from "@react-native-firebase/auth";
import firestore from "@react-native-firebase/firestore";
import * as RNIap from "react-native-iap";
import { SessionLanding } from "./../components/SessionLanding";
import {
  setAsyncSubscription,
  getAsyncSubscription,
} from "./../pages/utilities/SaveSubscription.js";
////////////////////////////3th////////////////////////////
import {
  List,
  Switch,
  InputItem,
  Button,
  WhiteSpace,
  Checkbox,
  PickerView,
} from "@ant-design/react-native";
//////////////////////////////////////////////////////////
import Icon from "react-native-vector-icons/AntDesign";
import Icon2 from "react-native-vector-icons/Feather";
import Icon3 from "react-native-vector-icons/FontAwesome5";
import analytics from "@react-native-firebase/analytics";
import crashlytics from "@react-native-firebase/crashlytics";
import { useRoute } from "@react-navigation/native";
import styles from "./ProfileScreen_Styles";
import {
  _rootInit,
  _root_initials,
  _root_initials_to_reset,
} from "./utilities/Global";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { BackgroundController } from "./../components/BackgroundController";
import {
  LoopAnim,
  ScaleAnim,
  EnterAnim,
  RotateAnim,
} from "./utilities/Animations";
const _root = _rootInit();
const seasons = [
  [
    {
      label: "Poca Experiencia",
      value: "principiante",
    },
    {
      label: "Mediana experiencia",
      value: "medio",
    },
    {
      label: "Mucha experiencia",
      value: "experto",
    },
  ],
];

const SettingsScreen = ({ navigation }) => {
  try {
    function LoginPlease() {
      return (
        <React.Fragment>
          <View style={styles.loginPlease}>
            <Image source={membersrRegister} style={styles.members}></Image>
            <Text style={styles.membersText}>
              Esta sección requiere una{" "}
              <Text style={styles.bold}>sesión iniciada</Text>.
            </Text>
          </View>
        </React.Fragment>
      );
    }

    const route = useRoute();
    const [header, setHeader] = useState(
      _root.headers.headerConfig.src + "?cache=" + Math.random()
    );
    const [valueEnabledRRSS, setValueEnabledRRSS] = useState(
      _root.user.enabledRRSS
    );
    const [valueList, setValueList] = useState([_root.user.experience]);
    const [enterRun, setEnterRun] = useState(false);
    const [modalVisible, setModalVisible] = useState(false);

    const [formUser, setFormUser] = useState({});

    function emailIsValid(getEmail) {
      if (getEmail) {
        let email = getEmail.trim();
        return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);
      }
    }

    async function logout() {
      Alert.alert("Cerrar sesión", `¿Quieres cerrar tu sesión ahora?`, [
        {
          text: "No",
          onPress: () => console.log("PROFILESCREEN--> Cancel Pressed"),
        },
        {
          text: "Sí",
          onPress: async () => {
            await auth()
              .signOut()
              .then(() => {
                // Sign-out successful.+
                _root_initials_to_reset();
                setAsyncSubscription();

                Toast.show({
                  type: "warning",
                  text1: "Listo.",
                  text2: "Tu sesión ha finalizado. 😧",
                  topOffset: 60,
                  visibilityTime: 2600,
                  autoHide: true,
                });
              })
              .catch((error) => {
                // An error happened.
                // console.log(
                //   "PROFILESCREEN--> No se ejecuto el cierre sessión!"
                // );
              });
            _root.entities.memberType = "Guest";
            navigation.navigate("HomeScreen", { action: "logout" });
          },
        },
      ]);
    }

    useEffect(() => {
      console.log("TIPO?", _root.entities.memberType, "RUTA", route.name);
      console.log(
        "TIPO 2?",
        global.UxApp.entities.memberType,
        "RUTA",
        route.name
      );
      if (
        _root.entities.mexwmberType == "Guest" &&
        route.name == "SettingsScreen"
      ) {
        // Alert.alert("Ups! config", `Esta sección requiere una sesión iniciada`, [
        //   {
        //     text: "No por ahora",
        //     onPress: () => navigation.goBack(),
        //   },
        //   {
        //     text: "Iniciar sesión",
        //     onPress: async () => {
        //       navigation.navigate("HomeScreen", {
        //         action: "suggestLogin",
        //       });
        //     },
        //   },
        // ]);
      }

      const unsubscribe = navigation.addListener("focus", () => {
        analytics().logScreenView({
          screen_class: route.name,
          screen_name: route.name,
        });
        setEnterRun(true);
      });
      const unsubscribeBlur = navigation.addListener("blur", () => {
        setEnterRun("off");
      });
      return unsubscribe, unsubscribeBlur;
    }, []);

    if (_root.entities.memberType !== "LoggedIn") {
      return <SessionLanding navigation={navigation}></SessionLanding>;
    }

    return (
      <BackgroundController>
        <Modal
          width={300}
          visible={modalVisible}
          // onTouchOutside={() => {
          //   setModalVisible(false);
          // }}
          modalAnimation={
            new SlideAnimation({
              slideFrom: "bottom",
            })
          }
          modalTitle={
            <ModalTitle
              title={
                <Text>
                  <Icon2 name="settings" size={18} color={"rgba(0,0,0,.3)"} />{" "}
                  Opciones
                </Text>
              }
            />
          }
          // swipeDirection={["up", "down", "left", "right"]}
          // onSwipeOut={(event) => {
          //   setModalVisible(false);
          // }}
          footer={
            <ModalFooter>
              <ModalButton
                text={"Volver"}
                onPress={() => {
                  setModalVisible(false);
                  navigation.goBack();
                }}
                textStyle={{
                  color: "rgba(0,0,0,.5)",
                }}
              />
              <ModalButton
                text={"Iniciar sesión"}
                onPress={() => {
                  setModalVisible(false);
                  setTimeout(() => {
                    navigation.navigate("HomeScreen", {
                      action: "suggestLogin",
                    });
                  }, 800);
                }}
                textStyle={{
                  color: "#809EFF",
                }}
              />
            </ModalFooter>
          }
        >
          <ModalContent>
            <LoginPlease></LoginPlease>
          </ModalContent>
        </Modal>

        <View style={styles.header}>
          {/* <View style={styles.mode}>
          <Switch
            trackColor={{ false: "#5A5A5A", true: "#5C5C5C" }}
            thumbColor={isEnabled ? "#8B8B8B" : "#f4f3f4"}
            ios_backgroundColor="#D4D4D4"
            onValueChange={toggleSwitch}
            value={isEnabled}
          />
        </View> */}
          <View style={styles.cover}>
            <Image
              style={styles.coverImage}
              source={{
                uri: header,
              }}
            />

            <View
              style={{
                width: 30,
                height: 30,
                position: "absolute",
                margin: 10,
                top: 0,
                left: 0,
              }}
            >
              <RotateAnim
                image={3}
                speed={6}
                reverse={false}
                run={enterRun}
                appear={12}
                appearReverse={true}
              />
            </View>
            <View
              style={{
                width: 50,
                height: 50,
                position: "absolute",
                margin: 30,
                top: 30,
                left: 30,
              }}
            >
              <RotateAnim
                run={enterRun}
                image={2}
                speed={6}
                reverse={false}
                appear={3}
                appearReverse={true}
              />
            </View>

            <View
              style={{
                width: 70,
                height: 70,
                position: "absolute",
                margin: 30,
                top: "-20%",
                left: "20%",
              }}
            >
              <RotateAnim
                run={enterRun}
                image={0}
                speed={10}
                reverse={false}
                appear={5}
                appearReverse={true}
              />
            </View>

            <View
              style={{
                width: 40,
                height: 40,
                position: "absolute",
                margin: 30,
                top: "48%",
                right: "10%",
              }}
            >
              <RotateAnim
                run={enterRun}
                image={5}
                speed={8}
                reverse={false}
                appear={30}
              />
            </View>
            <View
              style={{
                width: 30,
                height: 30,
                position: "absolute",
                margin: 30,
                top: "22%",
                left: "80%",
              }}
            >
              <RotateAnim
                run={enterRun}
                image={1}
                speed={12}
                reverse={true}
                appear={3}
              />
            </View>
            <View
              style={{
                width: 180,
                height: 180,
                position: "absolute",
                margin: 30,
                top: "42%",
                left: "70%",
              }}
            >
              <RotateAnim
                run={enterRun}
                image={4}
                speed={4}
                reverse={false}
                appear={14}
              />
            </View>
          </View>
        </View>

        <View style={styles.formProfile}>
          <View style={styles.formField}>
            <Button
              style={styles.formButtonSubscription}
              onPress={() => {
                analytics().logEvent("gotoSubscription", {
                  name: "gotoSubscription",
                  uniqId: _root.user.uniqId,
                  providerId: _root.user.providerId,
                  authId: _root.user.authId,
                  subscriptionType: _root.user.subscriptionType,
                  id: _root.user.id,
                });
                navigation.navigate("Suscripción");
              }}
            >
              <Icon name="rocket1" size={32} color={"#5100FF"} />
              {"  "}
              <Text style={styles.formTextSubscription}>Mi suscripción</Text>
            </Button>
          </View>
          {/* <View style={styles.formField}>
            <Button style={styles.formButton}>
              <Icon name="reload1" size={20} color={"#0098f0"} />
              {"  "}Restaurar suscripción{" "}
            </Button>
          </View> */}
          <View style={styles.formField}>
            <Button
              style={styles.formButton}
              onPress={() => {
                analytics().logShare({
                  content_type: "apple-review",
                  item_id: "apple-review",
                  method: "apple-review",
                });
                Linking.openURL(
                  //"itms://itunes.apple.com/in/app/apple-store/1558973980"
                  "https://apps.apple.com/app/id1558973980?action=write-review"
                ).catch((err) => console.error("Couldn't load page", err));

                // Toast.show({
                //   type: "done",
                //   text1: "Reseñas",
                //   text2:
                //     "Te estamos redireccionando a la App Store ⭐️⭐️⭐️⭐️⭐️...",
                //   topOffset: 60,
                //   visibilityTime: 3000,
                //   autoHide: true,
                //   onHide: () => {

                //   },
                // });
              }}
            >
              <Icon name="staro" size={20} color={"#0098f0"} />
              {"  "}Envíanos tu reseña{" "}
            </Button>
          </View>

          <View style={styles.formField}>
            <Button
              style={styles.formButton}
              onPress={() => {
                navigation.navigate("Contacto");
              }}
            >
              <Icon2 name="thumbs-up" size={20} color={"#0098f0"} />
              {"  "}Contacto{" "}
            </Button>
          </View>

          <View style={styles.formField}>
            <Button style={styles.formButton} onPress={logout}>
              <Icon name="logout" size={20} color={"#0098f0"} />
              {"  "}
              Cerrar mi sesión
            </Button>
          </View>

          <View style={styles.terms}>
            <Text
              style={styles.termsText}
              onPress={() => {
                analytics().logShare({
                  content_type: "privacy",
                  item_id: "privacy",
                  method: "privacy",
                });
                Linking.openURL("https://uxapp.com.mx/privacy").catch((err) =>
                  console.error("Couldn't load page", err)
                );
              }}
            >
              Aviso de privacidad |{" "}
            </Text>
            <Text
              style={styles.termsText}
              onPress={() => {
                analytics().logShare({
                  content_type: "terms",
                  item_id: "terms",
                  method: "terms",
                });
                Linking.openURL("https://uxapp.com.mx/terms").catch((err) =>
                  console.error("Couldn't load page", err)
                );
              }}
            >
              Términos y condiciones
            </Text>
          </View>
        </View>
      </BackgroundController>
    );
  } catch (e) {
    console.log("SETTINGSSCREEN--> ", e);
    return null;
    //crashlytics().crash();
  }
};
//https://github.com/oblador/react-native-vector-icons/blob/master/glyphmaps/AntDesign.json
//https://oblador.github.io/react-native-vector-icons/

export default SettingsScreen;
