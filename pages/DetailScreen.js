import React, { useState, useEffect, useRef } from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";
import Toast from "react-native-toast-message";

import {
  Modal,
  ModalPortal,
  SlideAnimation,
  ModalContent,
  ModalTitle,
  ModalFooter,
  ModalButton,
} from "react-native-modals";
import {
  View,
  Text,
  SectionList,
  Image,
  Pressable,
  Platform,
  Button,
  Animated,
  Easing,
  Alert,
  ScrollView,
} from "react-native";
////////////////////////////3th////////////////////////////
import analytics from "@react-native-firebase/analytics";
import crashlytics from "@react-native-firebase/crashlytics";
import { useRoute } from "@react-navigation/native";
import Icon2 from "react-native-vector-icons/Feather";
import Icon from "react-native-vector-icons/AntDesign";
import Item from "../components/Item";
import Modalito from "../components/Modalito";
import {
  saveFavorite,
  deleteFavorite,
  getFavorites,
} from "./utilities/SaveFavorite";
import { ScaleAnim, EnterAnim, RotateAnim } from "./utilities/Animations";
import { Grayscale } from "react-native-color-matrix-image-filters";

import moment from "moment";
import "moment-timezone";
moment().tz("America/Mexico_City").format();
//https://storage.googleapis.com/ux-tips-9e78f.appspot.com/post-1_1.png

import { _rootInit } from "./utilities/Global";
const _root = _rootInit();

import styles from "./DetailScreen_Styles";
import stylesInteraction from "./utilities/Animation_Styles";
import lock from "./../design/lock.png";
import unlock from "./../design/unlock.png";
import members from "./../design/members.png";
import membersrRegister from "./../design/membersrRegister.png";

import { BackgroundController } from "./../components/BackgroundController";

const DetailScreen = ({ navigation }) => {
  try {
    //
    const scrollRef = useRef();

    const route = useRoute();
    const [header, setHeader] = useState(
      _root.headers.header.src + "?cache=" + Math.random()
    );
    const [data, setData] = useState([]);
    const [modalVisible, setModalVisible] = useState(false);
    const [enterRun, setEnterRun] = useState(false);
    const [member, setMember] = useState(_root.entities.premium);
    const [isScrollEnabled, setIsScrollEnabled] = useState(true);

    useEffect(() => {
      const unsubscribe = navigation.addListener("focus", () => {
        analytics().logScreenView({
          screen_class: route.name,
          screen_name: route.name,
        });

        setEnterRun(true);
        setMember(_root.entities.premium);
        // console.log("wrapperOptions route.name ", route.name);
      });
      const unsubscribeBlur = navigation.addListener("blur", () => {
        setEnterRun("off");
      });
      prepareRender();
      return unsubscribe, unsubscribeBlur;
    }, []);

    function prepareRender() {
      if (_root.articles.length) {
        let renderArticles = [];
        _root.articles.map((item, index) => {
          renderArticles.push({
            image: `https://storage.googleapis.com/ux-tips-9e78f.appspot.com/${
              item.data.image
            }_1.png?cache=${moment()}`,
            image1: `https://storage.googleapis.com/ux-tips-9e78f.appspot.com/${
              item.data.image
            }_2.png?cache=${moment()}`,
            image2: `https://storage.googleapis.com/ux-tips-9e78f.appspot.com/${
              item.data.image
            }_3.png?cache=${moment()}`,
            imageReal: item.data.image,
            title: item.data.title,
            subtitle: item.data.subtitle,
            level: `Nivel ${item.data.level}`,
            realLevel: `${item.data.level}`,
            content: item.data.content,
            id: item.id,
            premium: item.data.premium,
            date: moment(item.data.date).format("L"),
          });
        });

        // console.log("VERESTRUCTURA****", renderArticles);
        setData(renderArticles);
      }
    }

    return (
      // <SafeAreaView style={styles.container}>

      <BackgroundController noscroll={true}>
        <Modalito
          modalVisible={modalVisible}
          setModalVisible={setModalVisible}
          navigation={navigation}
        ></Modalito>
        {/* <EnterAnim run={enterRun}> */}
        <View style={styles.header}>
          <View style={styles.cover}>
            <Image
              style={styles.coverImage}
              source={{
                uri: header,
              }}
            />
            <View style={stylesInteraction.interactiveLeft}>
              <RotateAnim
                run={enterRun}
                appear={12}
                image={0}
                speed={6}
                reverse={false}
              />
            </View>

            <View style={stylesInteraction.interactiveRight2}>
              <RotateAnim
                run={enterRun}
                appear={4}
                image={5}
                speed={2}
                reverse={true}
              />
            </View>
            <View style={stylesInteraction.interactiveLeft2}>
              <RotateAnim
                run={enterRun}
                appear={8}
                image={5}
                speed={3}
                reverse={false}
              />
            </View>

            <View style={stylesInteraction.interactiveRight3}>
              <RotateAnim
                run={enterRun}
                appear={8}
                image={3}
                speed={8}
                reverse={true}
              />
            </View>
          </View>
        </View>
        {/* </EnterAnim> */}
        {data.length > 0 && (
          <ScrollView
            style={styles.sectionList}
            ref={scrollRef}
            scrollEnabled={isScrollEnabled}
          >
            {data.map((item, index) => {
              return (
                <Item
                  content={item}
                  navigation={navigation}
                  setModalVisible={setModalVisible}
                  member={member}
                  goIntern={"FullDetailScreen"}
                  key={index}
                />
              );
            })}
          </ScrollView>
        )}
      </BackgroundController>
    );
  } catch (e) {
    console.log(e.message);
    return null;
    //crashlytics().crash();
  }
};

export default DetailScreen;
