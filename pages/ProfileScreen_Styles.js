import React, { useState, useEffect } from "react";
import {
  SafeAreaView,
  View,
  FlatList,
  StyleSheet,
  Text,
  StatusBar,
  SectionList,
  Switch,
  Image,
  Pressable,
  Platform,
} from "react-native";
export default styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  members: {
    width: "100%",
    resizeMode: "contain",
    height: 250,
    marginTop: 30,
  },
  membersText: {
    color: "rgba(0,0,0,.8)",
    marginTop: 20,
    textAlign: "center",
  },
  coverImage: {
    width: "100%",
    height: 300,
  },
  itemContainer: {
    display: "flex",
    flexDirection: "row",
    marginBottom: 15,
  },
  coverImage: {
    width: "100%",
    height: 300,
  },
  formProfile: {
    display: "flex",
    padding: 30,
    marginTop: 30,
  },
  formLabel: {
    fontSize: 18,
    color: "#809EFF",
  },
  formField: {
    marginBottom: 22,
    width: "100%",
  },
  formFieldGuest: {
    marginTop: 30,
    width: "100%",
  },
  formFieldPicker: {},

  formButton: {
    borderRadius: 18,
    width: "100%",
    height: 60,
    // backgroundColor: "#809EFF",
  },
  separator: {
    marginBottom: 60,
    marginTop: 60,
    width: "100%",
    height: 2,
    backgroundColor: "rgba(0,0,0,.1)",
  },
  formLabelTitle: {
    fontSize: 22,
    marginBottom: 30,
    color: "#809EFF",
  },
  formLabelSubsText: {
    fontSize: 18,
    color: "#809EFF",
    marginBottom: 60,
  },
  formLabelSubsTextSubs: {
    fontSize: 18,
    color: "rgba(0,0,0,.4)",
    marginBottom: 60,
  },
  formFieldSwitch: {
    marginTop: 20,
    marginBottom: 40,
  },
  formLabelSwitchText: {
    marginTop: 10,
    marginLeft: 10,
    color: "rgba(0,0,0,.6)",
  },
  terms: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 30,
    marginBottom: 10,
  },
  termsText: {
    fontSize: 14,
    marginTop: 20,
    color: "rgba(0,0,0,0.5)",
  },
  activityIndicator: {
    marginTop: 20,
  },
  formButtonSave: {
    borderRadius: 18,
    width: "100%",
    height: 60,
    color: "white",
    backgroundColor: "#809EFF",
    //display: 'flex',
  },
  formButtonSubscription: {
    borderRadius: 18,
    width: "100%",
    height: 60,
    color: "white",
    backgroundColor: "#00FFAA",
    borderColor: "#75FFF4",
    borderWidth: 3,
    //display: 'flex',
  },
  formTextSubscription: {
    fontSize: 22,
  },
  emptyGuestContainer: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    alignContent: "center",
    textAlign: "center",
    height: "80%",
  },
  emptyGuest: {
    width: "80%",
    height: "50%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    alignContent: "center",
  },
  broken: {
    color: "#75FFF4",
  },
  brokenText: {
    color: "rgba(0,0,0,0.4)",
    textAlign: "center",
    fontSize: 18,
    marginTop: 30,
  },
  bold: {
    fontWeight: "bold",
  },
  registerNow: {
    width: "100%",
    height: 400,
    resizeMode: "contain",
  },
  center: {
    width: "100%",
    height: "100%",
    display: "flex",
    alignItems: "center",
    alignContent: "center",
    justifyContent: "center",
  },
  centerTextContainer: {
    padding: 30,
    display: "flex",
    alignItems: "center",
    alignContent: "center",
    justifyContent: "center",
  },
  centerText: {
    fontSize: 18,
    color: "rgba(0,0,0,0.5)",
    textAlign: "center",
  },
  centerButton: {
    marginTop: 30,
  },
});
