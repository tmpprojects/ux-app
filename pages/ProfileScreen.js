import React, { useEffect, useState } from "react";
import {
  SafeAreaView,
  View,
  Text,
  ActivityIndicator,
  Image,
  Linking,
  Alert,
} from "react-native";
import {
  Modal,
  ModalPortal,
  SlideAnimation,
  ModalContent,
  ModalTitle,
  ModalFooter,
  ModalButton,
} from "react-native-modals";
import Toast from "react-native-toast-message";
import members from "./../design/members.png";
import membersrRegister from "./../design/membersrRegister.png";
import registerNow from "./../design/welcome-subscription.png";
import auth from "@react-native-firebase/auth";
import firestore from "@react-native-firebase/firestore";
import * as RNIap from "react-native-iap";
////////////////////////////3th////////////////////////////
import {
  List,
  Switch,
  InputItem,
  Button,
  WhiteSpace,
  Checkbox,
  PickerView,
} from "@ant-design/react-native";
//////////////////////////////////////////////////////////
import Icon from "react-native-vector-icons/AntDesign";
import Icon2 from "react-native-vector-icons/Feather";
import Icon3 from "react-native-vector-icons/FontAwesome5";
import analytics from "@react-native-firebase/analytics";
import crashlytics from "@react-native-firebase/crashlytics";
import { useRoute } from "@react-navigation/native";
import styles from "./ProfileScreen_Styles";
import { _rootInit } from "./utilities/Global";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { BackgroundController } from "./../components/BackgroundController";
import { SessionLanding } from "./../components/SessionLanding";
import {
  LoopAnim,
  ScaleAnim,
  EnterAnim,
  RotateAnim,
} from "./utilities/Animations";
const _root = _rootInit();
const seasons = [
  [
    {
      label: "Poca Experiencia",
      value: "principiante",
    },
    {
      label: "Mediana experiencia",
      value: "medio",
    },
    {
      label: "Mucha experiencia",
      value: "experto",
    },
  ],
];

const ProfileScreen = ({ navigation }) => {
  try {
    function LoginPlease() {
      return (
        <React.Fragment>
          <View style={styles.loginPlease}>
            <Image source={membersrRegister} style={styles.members}></Image>
            <Text style={styles.membersText}>
              Esta sección requiere una{" "}
              <Text style={styles.bold}>sesión iniciada</Text>.
            </Text>
          </View>
        </React.Fragment>
      );
    }

    const route = useRoute();
    const [header, setHeader] = useState(
      _root.headers.headerProfile.src + "?cache=" + Math.random()
    );
    const [valueEnabledRRSS, setValueEnabledRRSS] = useState(
      _root.user.enabledRRSS
    );
    const [valueList, setValueList] = useState([_root.user.experience]);
    const [enterRun, setEnterRun] = useState(false);
    const [modalVisible, setModalVisible] = useState(false);

    const [formUser, setFormUser] = useState({});

    useEffect(() => {
      console.log("TIPO: ", _root.entities.memberType, "RUTA", route.name);
      console.log("ENTITIES: ", _root.entities);

      if (_root.entities.memberType === "Guest" && route.name == "Mi perfil") {
        console.log("ENTITIES PERFIL: ", _root.entities);

        // Alert.alert("Perfil - Ups!", `Esta sección requiere una sesión iniciada${_root.entities.memberType}`, [
        //   {
        //     text: "No por ahora",
        //     onPress: () => navigation.goBack(),
        //   },
        //   {
        //     text: "Iniciar sesión",
        //     onPress: async () => {
        //       navigation.navigate("HomeScreen", {
        //         action: "suggestLogin",
        //       });
        //     },
        //   },
        // ]);
      }

      const unsubscribe = navigation.addListener("focus", () => {
        analytics().logScreenView({
          screen_class: route.name,
          screen_name: route.name,
        });
        setEnterRun(true);
      });
      const unsubscribeBlur = navigation.addListener("blur", () => {
        setEnterRun("off");
      });
      return unsubscribe, unsubscribeBlur;
    }, []);

    if (_root.entities.memberType !== "LoggedIn") {
      return <SessionLanding navigation={navigation}></SessionLanding>;
    }

    return (
      <BackgroundController>
        <Modal
          width={300}
          visible={modalVisible}
          // onTouchOutside={() => {
          //   setModalVisible(false);
          // }}
          modalAnimation={
            new SlideAnimation({
              slideFrom: "bottom",
            })
          }
          modalTitle={
            <ModalTitle
              title={
                <Text>
                  <Icon2 name="user" size={18} color={"rgba(0,0,0,.3)"} /> Mi
                  perfil
                </Text>
              }
            />
          }
          // swipeDirection={["up", "down", "left", "right"]}
          // onSwipeOut={(event) => {
          //   setModalVisible(false);
          // }}
          footer={
            <ModalFooter>
              <ModalButton
                text={"Volver"}
                onPress={() => {
                  setModalVisible(false);
                  navigation.goBack();
                }}
                textStyle={{
                  color: "rgba(0,0,0,.5)",
                }}
              />
              <ModalButton
                text={"Iniciar sesión"}
                onPress={() => {
                  setModalVisible(false);
                  setTimeout(() => {
                    navigation.navigate("HomeScreen", {
                      action: "suggestLogin",
                    });
                  }, 800);
                }}
                textStyle={{
                  color: "#809EFF",
                }}
              />
            </ModalFooter>
          }
        >
          <ModalContent>
            <LoginPlease></LoginPlease>
          </ModalContent>
        </Modal>

        <View style={styles.header}>
          {/* <View style={styles.mode}>
          <Switch
            trackColor={{ false: "#5A5A5A", true: "#5C5C5C" }}
            thumbColor={isEnabled ? "#8B8B8B" : "#f4f3f4"}
            ios_backgroundColor="#D4D4D4"
            onValueChange={toggleSwitch}
            value={isEnabled}
          />
        </View> */}
          <View style={styles.cover}>
            <Image
              style={styles.coverImage}
              source={{
                uri: header,
              }}
            />
            <View
              style={{
                width: 30,
                height: 30,
                position: "absolute",
                margin: 10,
                top: 0,
                left: 0,
              }}
            >
              <RotateAnim
                image={3}
                speed={6}
                reverse={false}
                run={enterRun}
                appear={12}
                appearReverse={true}
              />
            </View>
            <View
              style={{
                width: 50,
                height: 50,
                position: "absolute",
                margin: 30,
                top: 30,
                left: 30,
              }}
            >
              <RotateAnim
                run={enterRun}
                image={2}
                speed={6}
                reverse={false}
                appear={3}
                appearReverse={true}
              />
            </View>

            <View
              style={{
                width: 70,
                height: 70,
                position: "absolute",
                margin: 30,
                top: "-20%",
                left: "20%",
              }}
            >
              <RotateAnim
                run={enterRun}
                image={0}
                speed={10}
                reverse={false}
                appear={5}
                appearReverse={true}
              />
            </View>

            <View
              style={{
                width: 40,
                height: 40,
                position: "absolute",
                margin: 30,
                top: "48%",
                right: "10%",
              }}
            >
              <RotateAnim
                run={enterRun}
                image={5}
                speed={8}
                reverse={false}
                appear={30}
              />
            </View>
            <View
              style={{
                width: 30,
                height: 30,
                position: "absolute",
                margin: 30,
                top: "22%",
                left: "80%",
              }}
            >
              <RotateAnim
                run={enterRun}
                image={1}
                speed={12}
                reverse={true}
                appear={3}
              />
            </View>
            <View
              style={{
                width: 180,
                height: 180,
                position: "absolute",
                margin: 30,
                top: "42%",
                left: "70%",
              }}
            >
              <RotateAnim
                run={enterRun}
                image={4}
                speed={4}
                reverse={false}
                appear={14}
              />
            </View>
          </View>
        </View>
        <View style={styles.formProfile}>
          <View style={styles.formField}>
            <Text style={styles.formLabel}>
              <Icon2 name="user" size={18} color={"#809EFF"}></Icon2> Nombre:
            </Text>
            <InputItem
              clear
              placeholder={"¿Cómo te podemos llamar?"}
              defaultValue={_root.user.aliasName}
              onChange={(value) => {
                setFormUser({ ...formUser, aliasName: value.trim() });
              }}
              placeholderTextColor={"rgba(0,0,0,.3)"}
              style={{ fontSize: 22 }}
              autoCapitalize={"words"}
              maxLength={50}
              returnKeyType={"done"}
              autoCorrect={false}
            ></InputItem>
          </View>
          <View style={styles.formField}>
            <Text style={styles.formLabel}>
              <Icon2 name="at-sign" size={18} color={"#809EFF"}></Icon2> Correo
              electrónico:
            </Text>
            <InputItem
              clear
              placeholder={"¿Cúal es tu correo?"}
              defaultValue={_root.user.aliasEmail}
              onChange={(value) => {
                setFormUser({ ...formUser, aliasEmail: value.trim() });
              }}
              placeholderTextColor={"rgba(0,0,0,.3)"}
              style={{ fontSize: 22 }}
              keyboardType={"email-address"}
              maxLength={50}
              textContentType="emailAddress"
              returnKeyType={"done"}
              autoCapitalize="none"
              autoCorrect={false}

              //onSubmitEditing={sendForm}
            ></InputItem>
          </View>
          <View style={styles.formField}>
            <Text style={styles.formLabel}>
              <Icon2 name="instagram" size={18} color={"#809EFF"}></Icon2>{" "}
              Instagram:
            </Text>

            <InputItem
              clear
              placeholder={_root.user.instagram}
              defaultValue={_root.user.instagram}
              onChange={(value) => {
                setFormUser({ ...formUser, instagram: value.trim() });
              }}
              keyboardType="default"
              autoCapitalize={"none"}
              placeholderTextColor={"rgba(0,0,0,.3)"}
              autoCorrect={false}
              maxLength={100}
              style={{ fontSize: 22 }}
              returnKeyType={"done"}
            ></InputItem>
          </View>
          <View style={styles.formField}>
            <Text style={styles.formLabel}>
              <Icon2 name="twitter" size={18} color={"#809EFF"}></Icon2>{" "}
              Twitter:
            </Text>
            <InputItem
              clear
              placeholder={_root.user.twitter}
              defaultValue={_root.user.twitter}
              onChange={(value) => {
                setFormUser({ ...formUser, twitter: value.trim() });
              }}
              keyboardType="default"
              autoCapitalize={"none"}
              placeholderTextColor={"rgba(0,0,0,.3)"}
              autoCorrect={false}
              maxLength={100}
              style={{ fontSize: 22 }}
              returnKeyType={"done"}
            ></InputItem>
          </View>
          <View style={styles.formField}>
            <Text style={styles.formLabel}>
              <Icon2 name="linkedin" size={18} color={"#809EFF"}></Icon2>{" "}
              LinkedIn:
            </Text>
            <InputItem
              clear
              placeholder={_root.user.linkedin}
              onChange={(value) => {
                setFormUser({ ...formUser, linkedin: value.trim() });
              }}
              defaultValue={_root.user.linkedin}
              keyboardType="default"
              autoCapitalize={"none"}
              placeholderTextColor={"rgba(0,0,0,.3)"}
              autoCorrect={false}
              maxLength={100}
              style={{ fontSize: 22 }}
              returnKeyType={"done"}
            ></InputItem>
          </View>
          <View style={styles.formField}>
            <Text style={styles.formLabel}>
              <Icon2 name="external-link" size={18} color={"#809EFF"}></Icon2>{" "}
              Sitio web:
            </Text>
            <InputItem
              clear
              placeholder={_root.user.website}
              onChange={(value) => {
                setFormUser({ ...formUser, website: value.trim() });
              }}
              defaultValue={_root.user.website}
              keyboardType="url"
              autoCapitalize={"none"}
              placeholderTextColor={"rgba(0,0,0,.3)"}
              autoCorrect={false}
              maxLength={300}
              style={{ fontSize: 22 }}
              returnKeyType={"done"}
            ></InputItem>
          </View>
          <View style={styles.formFieldPicker}>
            <Text style={styles.formLabel}>
              ¿Que tanta experiencia tienes en UX?
            </Text>
            <PickerView
              onChange={(value) => {
                setValueList(value);
                setFormUser({
                  ...formUser,
                  experience: value.toString().trim(),
                });
              }}
              value={valueList}
              data={seasons}
              cascade={false}
            />
          </View>
          <View style={styles.formField}>
            <Text style={styles.formLabel}>
              ¿Te gustaría compartir tus redes sociales con la comunidad?
            </Text>
            <View style={styles.formFieldSwitch}>
              <Switch
                checked={valueEnabledRRSS}
                onChange={(value) => {
                  setValueEnabledRRSS(value);
                  setFormUser({ ...formUser, enabledRRSS: value });
                }}
                color={"#809EFF"}
              ></Switch>
              <Text style={styles.formLabelSwitchText}>
                ({" "}
                {valueEnabledRRSS ? (
                  <React.Fragment>
                    <Icon2
                      name="smile"
                      size={18}
                      color={"rgba(0,0,0,.4)"}
                    ></Icon2>
                    {" Sí"}
                  </React.Fragment>
                ) : (
                  <React.Fragment>
                    <Icon2
                      name="frown"
                      size={18}
                      color={"rgba(0,0,0,.4)"}
                    ></Icon2>
                    {" No"}
                  </React.Fragment>
                )}{" "}
                )
              </Text>
            </View>
          </View>

          <View style={styles.formField}>
            <Button
              style={styles.formButtonSave}
              type="primary"
              activeStyle={{ backgroundColor: "#809EFF" }}
              onPress={hotSave}
            >
              <Icon name="form" size={20} color={"white"} />
              {"  "}
              Guardar
            </Button>
          </View>
        </View>
      </BackgroundController>
    );

    function hotSave() {
      // let validateUser = formUser.aliasEmail;
      // //emailIsValid(validateUser)
      if (true) {
        updateUserData(formUser);
        Toast.show({
          type: "done",
          text1: "Actualización de perfil",
          text2: "Tu cambios se guardarón correctamente. 🎉",
          topOffset: 60,
          visibilityTime: 2500,
          autoHide: true,
          onHide: () => {
            console.log("salir");
          },
        });
      } else {
        Toast.show({
          type: "error",
          text1: "Error al actualizar.",
          text2: "Tu correo no es válido. 😨",
          topOffset: 60,
          visibilityTime: 2200,
          autoHide: true,
        });
      }
    }

    async function updateUserData({
      aliasName,
      aliasEmail,
      instagram,
      experience,
      twitter,
      linkedin,
      website,
      enabledRRSS,
    }) {
      // console.log("*******>", formUser);
      let onlyUpdateFields = {};
      if (Boolean(aliasName)) {
        _root.user.aliasName = aliasName;
        onlyUpdateFields.aliasName = aliasName;
      }
      if (Boolean(aliasEmail)) {
        _root.user.aliasEmail = aliasEmail;
        onlyUpdateFields.aliasEmail = aliasEmail;
      }
      if (Boolean(instagram)) {
        _root.user.instagram = instagram;
        onlyUpdateFields.instagram = instagram;
      }
      if (Boolean(twitter)) {
        _root.user.twitter = twitter;
        onlyUpdateFields.twitter = twitter;
      }
      if (Boolean(linkedin)) {
        _root.user.linkedin = linkedin;
        onlyUpdateFields.linkedin = linkedin;
      }
      if (Boolean(website)) {
        _root.user.website = website;
        onlyUpdateFields.website = website;
      }
      if (Boolean(experience)) {
        _root.user.experience = experience;
        onlyUpdateFields.experience = experience;
      }

      _root.user.enabledRRSS = enabledRRSS;
      onlyUpdateFields.enabledRRSS = enabledRRSS;

      if (Object.values(onlyUpdateFields).length) {
        if (_root.user.authId) {
          // console.log("buscando...", _root.user.authId);
          const membersRef = firestore().collection("members");
          const snapshot = await membersRef
            .where("authId", "==", _root.user.authId)
            .get();
          // const snapshot = await membersRef
          // .where("email", "==", _root.user.email)
          // .get();

          if (!snapshot.empty) {
            let infoCurrentUser = [];
            // Ya existia anteriormente
            snapshot.forEach((doc) => {
              infoCurrentUser = {
                ...doc.data(),
                id: doc.id,
              };
            });

            const updateDoc = firestore()
              .collection("members")
              .doc(infoCurrentUser.id);

            updateDoc
              .update(onlyUpdateFields)
              .then(() => {
                console.log("Document successfully updated!", onlyUpdateFields);
              })
              .catch((e) => {
                crashlytics().recordError(e);
                // The document probably doesn't exist.
                console.log("Error updating document: ", e);
              });
            console.log("RECUPERAR LA INFO", infoCurrentUser);
          }
        }
      }
    }
  } catch (e) {
    crashlytics().recordError(e);
    console.log("HEADERS-->", _root.headers);
    console.log("PROFILESCREEN--> ", e);
    return null;
    //crashlytics().crash();
  }
};
//https://github.com/oblador/react-native-vector-icons/blob/master/glyphmaps/AntDesign.json
//https://oblador.github.io/react-native-vector-icons/

export default ProfileScreen;
