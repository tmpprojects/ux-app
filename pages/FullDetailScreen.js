import React, { useState, useEffect } from "react";
import {
  SafeAreaView,
  View,
  FlatList,
  StyleSheet,
  Text,
  StatusBar,
  SectionList,
  Switch,
  Image,
  ScrollView,
  Dimensions,
  Linking,
  ImageBackground,
} from "react-native";
import Toast from "react-native-toast-message";
////////////////////////////3th////////////////////////////
import analytics from "@react-native-firebase/analytics";
import crashlytics from "@react-native-firebase/crashlytics";
import { useRoute } from "@react-navigation/native";

import styles from "./FullDetailScreen_Styles";
import Carousel, { Pagination } from "react-native-snap-carousel";
import { _rootInit } from "./utilities/Global";
import HTML from "react-native-render-html";
import exclusiveContent from "./../design/exclusive-content.png";
import headerBackground from "./../design/header-background.png";
import { BackgroundController } from "./../components/BackgroundController";
const _root = _rootInit();
const windowWidth = Dimensions.get("window").width;
const windowHeight = Dimensions.get("window").height;

let _carousel;
const _renderItem = ({ item, index }) => {
  // console.log("MISREAE", item.image);
  return (
    <View>
      {/* <Text style={styles.title}>{item.title}</Text> */}
      <Image
        style={styles.imageTinder}
        source={{
          uri: item.image,
        }}
      />
    </View>
  );
};

const FullDetailScreen = ({ navigation }) => {
  try {
    const route = useRoute();

    const [isEnabled, setIsEnabled] = useState(false);
    const toggleSwitch = () => setIsEnabled((previousState) => !previousState);
    const [activeSlide, setActiveSlide] = useState(0);
    const [entries, setEntries] = useState([]);
    const [showSlider, setShowSlider] = useState(false);

    useEffect(() => {
      setShowSlider(true);
    }, [entries]);

    useEffect(() => {
      // console.log(entries);
      if (_root.user.member == "true" && _root.selected.premium == true) {
        navigation.setOptions({ headerTitle: "Suscríbete ⚡️" });
      } else {
        navigation.setOptions({ headerTitle: "UX App" });
      }
      //////////////////////////////Analytics
      const unsubscribe = navigation.addListener("focus", () => {
        // console.log("actualizar");
        if (_root.selected) {
          setEntries([
            {
              title: "imagen1",
              image: _root.selected.image1,
            },
            {
              title: "imagen2",
              image: _root.selected.image2,
            },
          ]);
        }

        analytics().logScreenView({
          screen_class: route.name,
          screen_name: route.name,
        });
      });
      return unsubscribe;
    }, []);
    return (
      <BackgroundController>
        <View style={styles.textContainer}>
          {/* <ImageBackground
            source={headerBackground}
            style={styles.headerBackground}
          > */}
          <View style={styles.fullDetailTitleContainer}>
            <Text
              style={
                _root.selected.premium
                  ? styles.fullDetailTitlePremium
                  : styles.fullDetailTitle
              }
            >
              {_root.selected.title}
            </Text>
          </View>
          {/* </ImageBackground> */}
        </View>

        {showSlider && entries.length > 0 && (
          <View style={styles.carousel}>
            <Carousel
              ref={(c) => {
                _carousel = c;
              }}
              layout={"default"}
              data={entries}
              renderItem={_renderItem}
              sliderWidth={windowWidth}
              itemWidth={400}
              onSnapToItem={(index) => setActiveSlide(index)}
              removeClippedSubviews={false}
              loop={true}
              activeAnimationType={"decay"}
              inactiveSlideOpacity={0}
              inactiveSlideScale={0.4}
            />

            <Pagination
              dotsLength={entries.length}
              activeDotIndex={activeSlide}
              containerStyle={{ backgroundColor: "rgba(0, 0, 0, 0)" }}
              dotStyle={{
                width: 10,
                height: 10,
                borderRadius: 5,
                marginHorizontal: 8,
                backgroundColor: "rgba(0, 0, 0, 0.32)",
              }}
              inactiveDotStyle={
                {
                  // Define styles for inactive dots here
                }
              }
              inactiveDotOpacity={0.4}
              inactiveDotScale={0.6}
            />
          </View>
        )}

        <View style={styles.textContainer}>
          {/* <Text style={styles.textTitle}>{_root.selected.title}</Text> */}
          {_root.user.member == "true" && _root.selected.premium == true && (
            <View style={styles.exclusiveContentContainer}>
              <Image
                source={exclusiveContent}
                style={styles.exclusiveContent}
              ></Image>
            </View>
          )}

          <HTML
            tagsStyles={stylesHTML}
            source={{ html: _root.selected.content }}
            baseFontStyle={{ fontSize: 18 }}
            imagesInitialDimensions={{ width: windowWidth, height: 500 }}
            ignoredTags={["style"]}
            ignoredStyles={["background-color", "color"]}
          />
        </View>
        <View style={styles.separatorLine} />

        <View style={styles.byContainer}>
          <View style={styles.byImage}>
            <Image
              source={{ uri: _root.selected.image }}
              style={styles.by}
            ></Image>
          </View>

          <View style={styles.byInfo}>
            <Text style={styles.byContactQuestion}>
              Dudas, comentarios y/o aportes:
            </Text>
            <Text style={styles.byContact}>
              Súmate a la comunidad de UX en{" "}
              <Text
                style={styles.byInstagram}
                onPress={() => {
                  analytics().logShare({
                    content_type: "instagram",
                    item_id:"instagram",
                    method:"instagram"
                  });

                  Linking.openURL(
                    "https://www.instagram.com/uxapp.com.mx"
                  ).catch((e) => console.error("Couldn't load page", e));
                }}
              >
                Instagram
              </Text>
            </Text>
            <Text style={styles.byContact}>
              Nos encantaría saber como podemos mejorar esta guía por medio de
              tu{" "}
              <Text
                style={styles.byEmail}
                onPress={() => {
                  analytics().logShare({
                    content_type: "apple-review",
                  item_id:"apple-review",
                  method:"apple-review"
                  });

                  Linking.openURL(
                    //"itms://itunes.apple.com/in/app/apple-store/1558973980"
                    "https://apps.apple.com/app/id1558973980?action=write-review"
                  ).catch((e) =>{
                    crashlytics().recordError(e);
                    console.error("Couldn't load page", e)
                  }
                   
                  );
                  // Toast.show({
                  //   type: "done",
                  //   text1: "Reseñas",
                  //   text2:
                  //     "Te estamos redireccionando a la App Store ⭐️⭐️⭐️⭐️⭐️...",
                  //   topOffset: 60,
                  //   visibilityTime: 3000,
                  //   autoHide: true,
                  //   onHide: () => {
                    
                  //   },
                  // });
                }}
              >
                reseña en la App Store.
              </Text>
              {/* https://apps.apple.com/us/app/doorhub-driver/uxapp.com.mx */}
            </Text>
          </View>
        </View>
      </BackgroundController>
    );
  } catch (e) {
    crashlytics().recordError(e);
    //crashlytics().crash();
  }
};
//------------------------------CSS---------------------------------
const stylesHTML = StyleSheet.create({
  a: {
    fontWeight: "300",
    color: "#1DBCC7", // make links coloured pink
  },
  p: {
    fontWeight: "300",
    color: "rgba(0,0,0,0.6)", // make links coloured pink
  },
  b: {
    fontWeight: "bold",
    color: "rgba(0,0,0,0.8)", // make links coloured pink
  },
  strong: {
    fontWeight: "bold",
    color: "rgba(0,0,0,.6)", // make links coloured pink
  },
});
export default FullDetailScreen;
