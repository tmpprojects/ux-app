import {
  SafeAreaView,
  View,
  FlatList,
  StyleSheet,
  Text,
  StatusBar,
  Button,
  ActivityIndicator,
  Modal,
  Pressable,
  Dimensions,
} from "react-native";
const windowWidth = Dimensions.get("window").width;
const windowHeight = Dimensions.get("window").height;
export default styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
    display: "flex",
    justifyContent: "flex-start",
  },
  logo: {
    width: 500,
    height: 240,
    resizeMode: "contain",
  },
  item: {
    backgroundColor: "#f9c2ff",
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },

  backgroundVideo: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  titleLogin: {
    fontSize: 18,
    marginBottom: 60,
    marginTop: 60,
    color: "#636363",
  },
  loginContainer: {
    display: "flex",
    alignItems: "center",
    alignContent: "center",
  },
  separator: {
    marginBottom: 16,
  },
  loginButtonsContainer: {
    //backgroundColor:'rgba(0,0,0,.1)',
    alignItems: "center",
    marginTop: 30,
    width: windowWidth - 90,
  },
  errorContainer: {
    alignItems: "center",
    display: "flex",
    marginTop: "50%",
    padding: 60,
  },
  errorImage: {
    width: 200,
    height: 200,
    resizeMode: "contain",
    borderRadius: 150,
    marginBottom: 30,
    justifyContent: "center",
  },
  errorText: {
    fontSize: 14,
    color: "rgba(0,0,0,.5)",
    textAlign: "center",
  },
  errorText2: {
    fontSize: 18,
    color: "#9E6DFF",
    textAlign: "center",
    marginTop: 30,
    fontWeight: "bold",
    marginBottom: 30,
  },
  welcome: {
    display: "flex",
    alignItems: "center",
    alignContent: "center",
    justifyContent: "center",
    height: "100%",
    width: "100%",
  },
});
