import {
  SafeAreaView,
  View,
  FlatList,
  StyleSheet,
  Text,
  StatusBar,
  Button,
  ActivityIndicator,
  Modal,
  Pressable,
} from "react-native";
export default styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  sectionList: {
    marginTop: 0,
    margin: 15,
  },
  item: {
    backgroundColor: "#f9c2ff",
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  textTitle: {
    fontSize: 30,
    marginBottom: 40,
  },
  description: {
    fontSize: 18,
    color: "#555555",
  },
  moreDescription: {
    fontSize: 14,
    color: "#A6A6A6",
  },
  textContainer: {
    padding: 15,
  },
  textDescription: {
    fontSize: 22,
    color: "#505050",
  },
  sectionList: {
    marginTop: 30,
    margin: 15,
  },
  headerSectionList: {},
  headerSectionListText: {
    fontSize: 20,
    padding: 3,
    textAlign: "right",
    fontSize: 16,
    color: "#A6A6A6",
  },
  carousel: {
    backgroundColor: "white",
  },
  coverImage: {
    width: "100%",
    height: 400,
  },
  itemContainer: {
    display: "flex",
    flexDirection: "row",
    marginBottom: 15,
  },
  itemImage: {
    width: 130,
    height: 130,
  },
  mode: {
    display: "flex",
    alignItems: "flex-end",
    padding: 15,
  },
  itemInfo: {
    flex: 1,
    marginLeft: 15,
  },
  imageTinder: {
    width: "100%",
    height: 300,
  },

  byContainer: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    marginTop: 30,
    marginBottom: 60,
  },
  by: {
    width: 100,
    height: 100,
    resizeMode: "contain",
    borderRadius: 200,
    marginLeft: 16,
  },
  byImage: {
    width: 100,
  },
  byInfo: {
    flex: 1,
    marginLeft: 40,
    paddingRight: 16,
  },
  byContactQuestion: {
    marginBottom: 12,
    color: "rgba(0,0,0,0.6)",
  },
  byContact: {
    marginTop: 6,
    color: "rgba(0,0,0,0.6)",
  },
  byInstagram: {
    color: "#809EFF",
    fontWeight: "bold",

  },
  byEmail: {
    color: "#809EFF",
    fontWeight: "bold",
  },
  separatorLine: {
    borderBottomColor: "rgba(0,0,0,.1)",
    borderBottomWidth: 1,
    marginBottom: 10,
    marginTop: 60,
  },
  exclusiveContentContainer: {
    display: "flex",
    //alignItems: 'flex-end'
  },
  exclusiveContent: {
    width: 150,
    height: 50,
    resizeMode: "contain",
    marginRight: 10,
  },

  fullDetailTitleContainer: {
    flex: 1,
    marginTop: 30,
  },
  fullDetailTitle: {
    color: "#809EFF",
    fontSize: 30,
    marginBottom: 30,
  },
  fullDetailTitlePremium: {
    color: "#EF9A00",
    fontSize: 30,
    marginBottom: 30,
  },
  headerBackground: {
    flex: 1,
    resizeMode: "stretch",
    justifyContent: "center",
    paddingTop: 30,
    paddingBottom: 30,
  },
});
