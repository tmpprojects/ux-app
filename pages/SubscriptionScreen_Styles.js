import React, { useState, useEffect } from "react";
import {
  SafeAreaView,
  View,
  FlatList,
  StyleSheet,
  Text,
  StatusBar,
  SectionList,
  Switch,
  Image,
  Pressable,
  Platform,
} from "react-native";
export default styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  restorePurchasesContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 20,
    marginBottom: 20,
  },
  restorePurchasesText: {
    marginBottom: 10,
    fontSize: 16,
    color: "rgba(0, 0, 0,0.6)",
  },
  backgroundVideo:{
    width:'100%',
    height:'100%',
  },

  itemContainer: {
    display: "flex",
    flexDirection: "row",
    marginBottom: 15,
  },
  coverImage: {
    width: "100%",
    height: 500,
  },
  coverVideo:{
    width: "100%",
    height: 500,
  },
  formLabel: {
    fontSize: 18,
    color: "#809EFF",
    marginBottom: 12,
  },
  formLabelTitle: {
    fontSize: 18,
    color: "rgba(0, 0, 0,0.6)",
    marginBottom: 12,
  },
  formLabelTitleThankYou:{
    color: '#809EFF',
    fontSize: 22,
    fontWeight: "bold",
  },
  formLabelTitleExpired:{
    color: 'red',
    fontWeight: "bold"
  },
  formLabelTitleActive:{
    color: '#809EFF',
  },
  infoContact: {
    fontSize: 18,
    color: "rgba(0,0,0,.6)",
    marginBottom: 18,
  },
  bold: {
    fontWeight: "bold",
  },
  boldActive: {
    fontWeight: "bold",
    color: '#809EFF',
  },
  formField: {
    marginBottom: 22,
  },
  formFieldLoad: {
    marginBottom: 22,
    display: "flex",
    alignItems: "center",
  },
  formFieldLoadActivity:{
    marginTop: 26,
  },

  activityIndicator: {
    marginTop: 20,
  },
  formButtonSave: {
    borderRadius: 18,
    width: "100%",
    height: 60,
    color: "white",
    backgroundColor: "#809EFF",
    //display: 'flex',
  },
  contactContainer: {
    padding: 30,

    display: "flex",
  },
  titleSpace: {
    marginBottom: 16,
    color: "red",
  },
  ////////////////////////////////////////////////////////////////
  membersText: {
    color: "rgba(0,0,0,.8)",
    marginTop: 20,
    textAlign: "center",
  },
  separatorLine: {
    borderBottomColor: "rgba(0,0,0,.1)",
    borderBottomWidth: 1,
    marginBottom: 10,
  },
  optionsContainer: {
    marginTop: 20,
    marginBottom: 10,
  },

  options: {
    display: "flex",
    flexDirection: "row",
    marginBottom: 16,
    alignItems: "center",
  },
  optionsIcon: {
    width: 100,
  },
  optionDescription: {
    marginLeft: 16,
    flex: 1,
  },
  optionDescriptionText: {
    color: "rgba(0,0,0,.5)",
  },
  optionIconImage: {
    width: 20,
    resizeMode: "contain",
    height: 20,
  },
  suscriptionContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
  },
  suscriptionImage: {
    width: "90%",
    resizeMode: "contain",
    height: 70,
  },
  subscriptionInfo: {
    textAlign: "center",
    backgroundColor: "#8000F8",
    borderRadius: 18,
    padding: 20,
    marginTop: 30,
    paddingTop: 20,
    paddingBottom: 20,
  },
  subscriptionInfoTitle: {
    textAlign: "center",
    fontSize: 18,
    fontWeight: "bold",
    color: "white",
    marginBottom: 10,
  },
  subscriptionInfoTitle2: {
    textAlign: "center",
    fontSize: 16,
    color: "#F6AAFF",
    marginBottom: 20,
  },
  subscriptionInfoTitle3: {
    textAlign: "center",
    fontSize: 18,
    fontWeight: "bold",
    color: "white",

  },
  subscriptionInfoTitleButton:{
    marginBottom: 10,
    backgroundColor: '#0F97FF',
    padding: 16,
    borderRadius: 18,
    borderWidth:3,
    borderColor: '#F7A8FF',

    shadowColor: "#FFA8FC",
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.39,
    shadowRadius: 8.3,
  },
  subscriptionInfoTitle4: {
    textAlign: "center",
    fontSize: 16,
    fontWeight: "normal",
    color: "#F6AAFF",
    marginBottom: 0,
  },
  subscriptionInfoTitle5: {
    textAlign: "center",
    fontSize: 16,
    fontWeight: "normal",
    color: "#0098f0",
    marginTop: 20,
  },
  subscription: {
    width: "100%",
    height: 70,
    resizeMode: "contain",
    
    ////////////////////////////////

    shadowColor: "rgba(0,0,0,.9)",
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.2,
    shadowRadius: 9,
    //elevation: 20,
  },
  subscriptionPressable: {
    display: "flex",
    alignItems: "center",
    marginBottom: 20,
    marginTop: 20,
  },
  terms: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 10,
  },
  termsText: {
    fontSize: 14,
    marginTop: 0,
    marginBottom: 30,
    color: "rgba(0,0,0,0.5)",
  },
  termsItunesText: {
    fontSize: 12,
    marginTop: 0,
    color: "rgba(0,0,0,0.3)",
  },
  activityIndicator: {
    marginTop: 20,
    height: 70,
  },
  bold: {
    fontWeight: "bold",
  },
});
