import React, { useEffect, useState } from "react";
import {
  SafeAreaView,
  View,
  Text,
  ActivityIndicator,
  Image,
  Linking,
  Alert,
  Platform,
  Pressable,
  NativeModules,
} from "react-native";
const { InAppUtils } = NativeModules;
import Toast from "react-native-toast-message";
import firestore from "@react-native-firebase/firestore";
import moment from "moment";
import "moment-timezone";
import Video from "react-native-video";
import {
  setAsyncSubscription,
  getAsyncSubscription,
  setAsyncExpired,
} from "./utilities/SaveSubscription.js";
import config from "./../env";
////////////////////////////3th////////////////////////////
import analytics from "@react-native-firebase/analytics";
import crashlytics from "@react-native-firebase/crashlytics";
import { useRoute } from "@react-navigation/native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import lock from "./../design/lock.png";
import unlock from "./../design/unlock.png";
import members from "./../design/members.png";
import exclusive from "./../design/exclusive.png";
import subscription from "./../design/suscription.png";
import icon1 from "./../content/747818-ui-interface/png/140-sun.png";
import icon2 from "./../content/747818-ui-interface/png/061-puzzle.png";
import icon3 from "./../content/747818-ui-interface/png/145-upload.png";
import icon4 from "./../content/747818-ui-interface/png/053-heart.png";
import icon5 from "./../content/747818-ui-interface/png/002-smile-1.png";
import headerSubscription from "./../design/header-subscription.png";
import headerSubscriptionActive from "./../design/header-subscription-active.png";
import styles from "./SubscriptionScreen_Styles";
import axios from "react-native-axios";
import video from "./../files/subs2.mp4";

import { validateRemoteReceipt } from "./../pages/utilities/ValidateReceipt";

import { BackgroundController } from "./../components/BackgroundController";
import { _rootInit } from "./utilities/Global";
const _root = _rootInit();
import * as RNIap from "react-native-iap";

const productIds = Platform.select({
  ios: ["mx.com.uxapp.member"],
  android: [""],
});
const UXMemberProduct = "mx.com.uxapp.member";

const SubscriptionScreen = ({ navigation }) => {
  try {
    const [currency, setCurrency] = useState("");
    const [title, setTitle] = useState("");

    const [description, setDescription] = useState("");
    const [price, setPrice] = useState("");
    const [localizedPrice, setLocalizedPrice] = useState("");
    const [loadProducts, setLoadProducts] = useState(false);
    const [subscriptionStatus, setSubscriptionStatus] = useState("none");
    const [dataSubscription, setDataSubscription] = useState({});
    const [hasServerError, setHasServerError] = useState(false);
    const [purchasing, setPurchasing] = useState(false);
    const [expireUnix, setExpireUnix] = useState(0);
    const [expireLocal, setExpireLocal] = useState(0);

    const route = useRoute();

    async function validate(receipt) {
      try {
        // console.log("valida la compra 👀--->", receipt.slice(0, 15));

        const receiptBody = {
          "receipt-data": receipt,
          password: config().password,
        };
        // console.log("haciendo pedición-->");
        // console.log("valida la compra (al cargar)--->", receipt.slice(0, 15));

        //"https://us-central1-ux-tips-9e78f.cloudfunctions.net/validateReceipt",
        // "http://localhost:5001/ux-tips-9e78f/us-central1/validateIos",

        // console.log("VAMOS A PROBAR-->", receiptBody);

        return axios
          .post(config().cloudFunction, receiptBody)
          .then((setResponse) => {
            console.log("F I R E B A S E - OK 💚", setResponse);
            setDataSubscription(setResponse);
            setExpireUnix(setResponse.data.expired_unix);
            if (Boolean(setResponse.data.expired_local)) {
              setExpireLocal(setResponse.data.expired_local);
            }

            return setResponse;
          })
          .catch((e) => {
            crashlytics().recordError(e);
            console.log("F I R E B A S E - ERROR 🧨", e);
            setHasServerError(true);
            return false;
          });
      } catch (e) {
        crashlytics().recordError(e);
        console.log("VALIDATE ERROR--->", e);
        return false;
      }
    }

    async function purchaseNow(sku) {
      console.log("ESPERA.... COMPRANDO....");
      setPurchasing(true);
      try {
        // console.log(sku);
        await RNIap.requestSubscription(sku);
      } catch (e) {
        crashlytics().recordError(e);
        setPurchasing(false);
        console.log(e.code, e.message);
      }
    }

    async function initialIAP(validatePurchase = false) {
      // console.log("intentando limpiar");
      await RNIap.clearTransactionIOS();

      RNIap.initConnection().then(async () => {
        try {
          setHasServerError(false);
          console.log("🧨 connected!");
          setLoadProducts(true);
          const getSubscription = await RNIap.getSubscriptions(
            productIds
          ).catch((e) => {
            crashlytics().recordError(e);
            return false;
          });
          if (getSubscription) {
            // Se encontraron productos de mi cuenta appStore para ofrecer:
            setCurrency(getSubscription[0].currency);
            setTitle(getSubscription[0].title); // "UX Member"
            setDescription(getSubscription[0].description); // "Includes all the contents of the app
            setPrice(getSubscription[0].price); // "Includes all the contents of the app
            setLocalizedPrice(getSubscription[0].localizedPrice); // "Includes all the contents of the app
            console.log(
              "🧨 tengo productos (items) que ofrecer",
              getSubscription
            );
            let getReceiptIOS;
            // if (Boolean(_root.user.receipt)) {
            //   console.log("RECEIPT LOCAL")
            //   getReceiptIOS = _root.user.receipt;
            // } else {
            //   console.log("RECEIPT FRAMEWORK")
            //   getReceiptIOS = await RNIap.getReceiptIOS();
            // }
            getReceiptIOS = await RNIap.getReceiptIOS();

            if (Boolean(getReceiptIOS) && validatePurchase) {
              console.log("Hay historico de compras");
              const receipt = getReceiptIOS;
              // console.log("historyPurchases", historyPurchases);
              // console.log("receipt", receipt);
              //const otherForm = await RNIap.getReceiptIOS();
              const returnApple = await validate(receipt);

              //console.log("otherForm getReceiptIOS---1", otherForm.slice(0, 15));
              console.log("receipt-------> ", receipt.slice(0, 15));
              // console.log("receipt appleTellsMe-------> ", returnApple);

              if (returnApple) {
                const dataApple = returnApple.data;

                if (dataApple.expired) {
                  // Toast.show({
                  //   type: "error",
                  //   text1: "Suscripción expirada 😔",
                  //   text2:
                  //     "¡Para volver a tener acceso a los contenidos intenta volver a suscribirte! 🎉",
                  //   topOffset: 60,
                  //   visibilityTime: 20000,
                  //   autoHide: true,
                  // });
                  disabledSubscribe();
                } else {
                  // console.log("2");
                  enabledSubscribe();
                }
              }
            } else {
              //No hay compras previas.
              console.log("Any subscription!");

              normalSubscribe();
            }
          }
        } catch (e) {
          setLoadProducts(false);
          setPurchasing(false);
          crashlytics().recordError(e);
          //If cancel login user.
          // console.log("e.message -->", e.message);
        }
      });
    }

    async function enabledSubscribe() {
      const typeSubs = "active";
      const getReceiptIOS = await RNIap.getReceiptIOS();
      setSubscriptionStatus(typeSubs);
      //setSubscriptionStatus("none");
      setAsyncSubscription(typeSubs, getReceiptIOS);
      setLoadProducts(false);
      setPurchasing(false);
      _root.entities.subscriptionType = typeSubs;
      _root.entities.premium = true;
      ///////////////////////UPDATE check_remote_subscription FIREBASE //////////////////////

      const updateDoc = firestore().collection("members").doc(_root.user.id);
      updateDoc.update({
        subscriptionType: "active",
      });
    }
    function disabledSubscribe() {
      const typeSubs = "expired";
      setSubscriptionStatus(typeSubs);
      //setSubscriptionStatus("none");
      setAsyncExpired(typeSubs);
      setLoadProducts(false);
      setPurchasing(false);
      _root.entities.subscriptionType = typeSubs;
      _root.entities.premium = false;
      ///////////////////////UPDATE check_remote_subscription to expired FIREBASE //////////////////////
      const updateDoc = firestore().collection("members").doc(_root.user.id);
      updateDoc.update({
        subscriptionType: typeSubs,
      });
    }
    function normalSubscribe() {
      setLoadProducts(false);
      setPurchasing(false);
    }

    useEffect(() => {
      ////////////////////////////////////////////

      ////////////

      initialIAP();
      ////////////

      ///////////////////////////////////////////////
      let purchaseUpdateSubscription = RNIap.purchaseUpdatedListener(
        async (purchase) => {
          try {
            const receipt = purchase.transactionReceipt;
            if (receipt) {
              await validate(receipt);
              console.log("se completo ⭐️");

              // console.log("purchase", purchase);

              _root.user.receipt = receipt;

              const updateDoc = firestore()
                .collection("members")
                .doc(_root.user.id);
              // console.log("FIREBASE GUARDAR EN BD");

              //const appleTellsMe = await validate(receipt);

              enabledSubscribe();

              updateDoc
                .update({
                  receipt,
                  expire_local: expireLocal,
                  expire_unix: expireUnix,
                  subscriptionType: "active",
                })
                .then(async () => {
                  if (Platform.OS === "ios") {
                    await RNIap.finishTransactionIOS(purchase.transactionId);
                  }
                  //await RNIap.finishTransaction(purchase, true);
                  console.log(
                    "Document successfully updated! AND finish transaction"
                  );

                  await RNIap.clearTransactionIOS();

                  analytics().logEvent("finishPurchase", {
                    name: "finishPurchase",
                    uniqId: _root.user.uniqId,
                    providerId: _root.user.providerId,
                    authId: _root.user.authId,
                    subscriptionType: _root.user.subscriptionType,
                    id: _root.user.id,
                  });
                })
                .catch((e) => {
                  // The document probably doesn't exist.
                  crashlytics().recordError(e);
                  console.error("Error updating document: ", e);
                });

              ////////////////FIREBASE SAVE
            }
          } catch (e) {
            crashlytics().recordError(e);
            console.error("Error 234: ", e);
          }
        }
      );

      ////////////////////////////////////////////////////////
      let purchaseErrorSubscription = RNIap.purchaseErrorListener((error) => {
        if (error.code == "E_USER_CANCELLED") {
          //user canceled
          console.log("El usuario cancelo la operación");
          //setSubscriptionStatus(subscriptionStatus);
        } else {
          //Alert.alert("Ocurrió algún error con tu compra.");
          //setSubscriptionStatus(subscriptionStatus);
          //terminos y condiciones
        }
        // purchaseUpdateSubscription = null;
        // console.log("purchaseErrorListener", error.message);
        // console.log("purchaseErrorListener", error.code);
        // console.log("purchaseErrorListener", error);
      });
      ///////////////////////////////////////
      let unsubscribe = navigation.addListener("focus", () => {
        navigation.setOptions({ headerTitle: "Suscríbete ⚡️" });
        analytics().logScreenView({
          screen_class: route.name,
          screen_name: route.name,
        });
        ////////////

        getAsyncSubscription().then(async (result) => {
          if (result.subscription == "active") {
            //setSubscriptionStatus("active");
            setLoadProducts(true);
            await initialIAP(true);
            validateRemoteReceipt(_root.user.id);
            console.log("****** ACTIVE");
          } else {
            console.log("****** UNACTIVE");
          }
        });
        ////////////
      });

      return () => {
        if (purchaseUpdateSubscription) {
          purchaseUpdateSubscription.remove();
          purchaseUpdateSubscription = null;
        }
        if (purchaseErrorSubscription) {
          purchaseErrorSubscription.remove();
          purchaseErrorSubscription = null;
        }
        unsubscribe = null;
      };
    }, []);

    ////////////////////////////////////////////////////////////////////////////VISTAS
    function GetDetailsSubscription({ purchase = true }) {
      return (
        <React.Fragment>
          <View style={styles.optionsContainer}>
            <View style={styles.options}>
              <View style={styles.optionIcon}>
                <Image source={icon1} style={styles.optionIconImage}></Image>
              </View>
              <View style={styles.optionDescription}>
                <Text style={styles.optionDescriptionText}>
                  Acceso libre a todos los artículos premium (con candado).
                </Text>
              </View>
            </View>
            <View style={styles.options}>
              <View style={styles.optionIcon}>
                <Image source={icon1} style={styles.optionIconImage}></Image>
              </View>
              <View style={styles.optionDescription}>
                <Text style={styles.optionDescriptionText}>
                  Actualizaciones diarias de buenas prácticas en User Experience
                  / User Interface.
                </Text>
              </View>
            </View>

            <View style={styles.options}>
              <View style={styles.optionIcon}>
                <Image source={icon1} style={styles.optionIconImage}></Image>
              </View>
              <View style={styles.optionDescription}>
                <Text style={styles.optionDescriptionText}>
                  Recibe notificaciones push cuando exista un articulo premium
                  nuevo.
                </Text>
              </View>
            </View>

            <View style={styles.options}>
              <View style={styles.optionIcon}>
                <Image source={icon1} style={styles.optionIconImage}></Image>
              </View>
              <View style={styles.optionDescription}>
                <Text style={styles.optionDescriptionText}>
                  Recibe invitaciones por correo sobre eventos de UX / UI
                  online, webinars y seminarios.
                </Text>
              </View>
            </View>

            <View style={styles.options}>
              <View style={styles.optionIcon}>
                <Image source={icon1} style={styles.optionIconImage}></Image>
              </View>
              <View style={styles.optionDescription}>
                <Text style={styles.optionDescriptionText}>
                  Podrás añadir favoritos ilimitados.
                </Text>
              </View>
            </View>

            <View style={styles.options}>
              <View style={styles.optionIcon}>
                <Image source={icon1} style={styles.optionIconImage}></Image>
              </View>
              <View style={styles.optionDescription}>
                <Text style={styles.optionDescriptionText}>
                  Prioridad en soporte y resolución de dudas sobre los temas
                  abordados.
                </Text>
              </View>
            </View>

            <View style={styles.options}>
              <View style={styles.optionIcon}>
                <Image source={icon1} style={styles.optionIconImage}></Image>
              </View>
              <View style={styles.optionDescription}>
                <Text style={styles.optionDescriptionText}>
                  Participar con tus contenidos en la comunidad de UX App.
                </Text>
              </View>
            </View>

            {purchase && (
              <View style={styles.subscriptionInfo}>
                <Text style={styles.subscriptionInfoTitle}>
                  ¡Sé un Gurú del UX!
                </Text>
                <Text style={styles.subscriptionInfoTitle2}>
                  Suscríbete ahora y obtén todos los beneficios anteriores por:
                </Text>

                {!purchasing && (
                  <Pressable
                    onPress={() => {
                      purchaseNow(UXMemberProduct);
                      analytics().logEvent("purchaseNow", {
                        name: "purchaseNow",
                        uniqId: _root.user.uniqId,
                        providerId: _root.user.providerId,
                        authId: _root.user.authId,
                        subscriptionType: _root.user.subscriptionType,
                        id: _root.user.id,
                      });
                    }}
                    style={styles.subscriptionPressable}
                  >
                    <View style={styles.subscriptionInfoTitleButton}>
                      <Text style={styles.subscriptionInfoTitle3}>
                        {localizedPrice} / Mes
                      </Text>
                    </View>
                  </Pressable>
                )}
                <Text style={styles.subscriptionInfoTitle4}>
                  (Cancela en cualquier momento)
                </Text>
                {purchasing && (
                  <View style={styles.formFieldLoadActivity}>
                    <ActivityIndicator
                      size={"large"}
                      color={"#ffffff"}
                    ></ActivityIndicator>
                  </View>
                )}
              </View>
            )}
          </View>

          {purchase && (
            <View style={styles.restorePurchasesContainer}>
              <Text
                style={styles.restorePurchasesText}
                onPress={() => {
                  analytics().logEvent("restorePurchases", {
                    name: "restorePurchases",
                    uniqId: _root.user.uniqId,
                    providerId: _root.user.providerId,
                    authId: _root.user.authId,
                    subscriptionType: _root.user.subscriptionType,
                    id: _root.user.id,
                  });
                  initialIAP(true);
                }}
              >
                Restaurar mis compras
              </Text>
            </View>
          )}
          <View style={styles.terms}>
            <Text style={styles.termsItunesText}>
              La suscripción a UX App Premium se cargará a su tarjeta de crédito
              a través de su cuenta de iTunes. Su suscripción se renovará
              automáticamente cada mes a menos que cancele 24 horas antes del
              final del período actual. Puede cancelar la suscripción en la
              opción > Suscripciones en su cuenta de iTunes.
            </Text>
          </View>

          <View style={styles.terms}>
            <Text
              style={styles.termsText}
              onPress={() => {
                analytics().logShare({
                  content_type: "privacy",
                  item_id: "privacy",
                  method: "privacy",
                });
                Linking.openURL("https://uxapp.com.mx/privacy").catch((err) =>
                  console.error("Couldn't load page", err)
                );
              }}
            >
              Aviso de privacidad |{" "}
            </Text>
            <Text
              style={styles.termsText}
              onPress={() => {
                analytics().logShare({
                  content_type: "terms",
                  item_id: "terms",
                  method: "terms",
                });
                Linking.openURL("https://uxapp.com.mx/terms").catch((err) =>
                  console.error("Couldn't load page", err)
                );
              }}
            >
              Términos y condiciones
            </Text>
          </View>
        </React.Fragment>
      );
    }
    function GetContent() {
      if (hasServerError) {
        return (
          <BackgroundController noscroll={false}>
            <View style={styles.header}>
              <Text>Ocurrio un problema al cargar desde el server.</Text>
            </View>
            <Pressable
              onPress={() => {
                purchaseNow(UXMemberProduct);
              }}
              style={styles.subscriptionPressable}
            >
              <Image source={subscription} style={styles.subscription}></Image>
            </Pressable>
            <View style={styles.header}>
              <Text>DESCONOCIDO</Text>
              <Text>subscriptionStatus: {subscriptionStatus.toString()}</Text>
              <Text>hasServerError: {hasServerError.toString()}</Text>
              <Text>loadProducts: {loadProducts.toString()}</Text>
            </View>
          </BackgroundController>
        );
      }

      if (loadProducts) {
        return (
          <View style={styles.contactContainer}>
            <View style={styles.formFieldLoad}>
              <Text style={styles.formLabelTitle}>
                Comprobando suscripciones...
              </Text>
            </View>
            <View style={styles.formFieldLoadActivity}>
              <ActivityIndicator
                size={"large"}
                color={"#000000"}
              ></ActivityIndicator>
            </View>
            <GetDetailsSubscription></GetDetailsSubscription>
          </View>
        );
        // return (
        //   <BackgroundController noscroll={false}>
        //     <View style={styles.header}>
        //       <Text>Cargando productos. {loadProducts.toString()}</Text>
        //     </View>
        //   </BackgroundController>
        // );
      }

      if (!hasServerError && !loadProducts) {
        if (subscriptionStatus == "active") {
          return (
            <View style={styles.contactContainer}>
              <View style={styles.formField}>
                <Text style={styles.formLabelTitle}>
                  <Text style={styles.formLabelTitleThankYou}>
                    ¡Gracias por tu compra!
                  </Text>
                </Text>
              </View>
              <View style={styles.formField}>
                <Text style={styles.formLabelTitle}>
                  <Text style={styles.formLabelTitle}>
                    ¡Tienes una suscripción activa! a{" "}
                  </Text>
                  <Text style={styles.boldActive}>"{title}"</Text>
                </Text>
              </View>
              <View style={styles.formField}>
                <Text style={styles.formLabelTitle}>
                  <Text style={styles.boldActive}>
                    Estará activa hasta: {expireLocal}, tu suscripción incluye:
                  </Text>
                </Text>
              </View>
              <GetDetailsSubscription purchase={false}></GetDetailsSubscription>
            </View>
          );
        }
        if (subscriptionStatus == "none") {
          return (
            <BackgroundController noscroll={false}>
              <View style={styles.contactContainer}>
                <View style={styles.formField}>
                  <Text style={styles.formLabelTitle}>
                    Adquiere una suscripción a{" "}
                    <Text style={styles.bold}>"{title}"</Text> por sólo{" "}
                    <Text style={styles.bold}>
                      {localizedPrice} / mensuales
                    </Text>{" "}
                    para tener ¡acceso libre a todo el aprendizaje!
                  </Text>
                  <Text style={styles.formLabelTitle}>
                    La suscripcón incluye:
                  </Text>

                  <GetDetailsSubscription></GetDetailsSubscription>
                </View>
              </View>
            </BackgroundController>
          );
        }

        if (subscriptionStatus == "expired") {
          return (
            <View style={styles.contactContainer}>
              <Text style={styles.formLabelTitle}>
                <Text style={styles.formLabelTitle}>
                  Tienes una suscripción expirada desde{" "}
                  <Text style={styles.formLabelTitleExpired}>
                    {expireLocal}
                  </Text>
                  , puedes volver a suscribirte a{" "}
                </Text>
                <Text style={styles.bold}>"{title}"</Text> por sólo{" "}
                <Text style={styles.bold}>{localizedPrice} / mensuales</Text>,
                para tener ¡acceso libre a todo el aprendizaje!
              </Text>
              <Text style={styles.formLabelTitle}>La suscripcón incluye:</Text>
              <GetDetailsSubscription></GetDetailsSubscription>
            </View>
          );
        }
      }
      return (
        <View style={styles.header}>
          <Text>DESCONOCIDO</Text>
          <Text>ERROR: {subscriptionStatus.toString()}</Text>
          <Text>ERROR: {hasServerError.toString()}</Text>
          <Text>ERROR: {loadProducts.toString()}</Text>
        </View>
      );
    }

    ////////////////////////////////////////////////////////////////

    return (
      <BackgroundController noscroll={false}>
        <View style={styles.header}>
          {subscriptionStatus == "active" && (
            <View style={styles.coverVideo}>
              <Video
                source={video}
                style={styles.backgroundVideo}
                muted={true}
                repeat={true}
                resizeMode={"cover"}
                rate={1.0}
              />
            </View>
          )}

          {subscriptionStatus != "active" && (
            <View style={styles.cover}>
              <Image style={styles.coverImage} source={headerSubscription} />
            </View>
          )}
        </View>
        <GetContent></GetContent>
      </BackgroundController>
    );
  } catch (e) {
    crashlytics().recordError(e);
    //crashlytics().crash();
    console.log(
      "error dentro de la pantalla o aún no se cargan los contenidos",
      e
    );
    return (
      <View style={styles.contactContainer}>
        <View style={styles.formFieldLoad}>
          <Text style={styles.formLabelTitle}>
            Comprobando suscripciones...
          </Text>
        </View>
        <View style={styles.formFieldLoadActivity}>
          <ActivityIndicator
            size={"large"}
            color={"#000000"}
          ></ActivityIndicator>
        </View>
      </View>
    );
  }
};

export default SubscriptionScreen;
