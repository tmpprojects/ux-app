import React, { useEffect, useState } from "react";
import {
  SafeAreaView,
  View,
  Text,
  ActivityIndicator,
  Image,
  Linking,
} from "react-native";
////////////////////////////3th////////////////////////////
import analytics from "@react-native-firebase/analytics";
import crashlytics from "@react-native-firebase/crashlytics";
import { useRoute } from "@react-navigation/native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Icon from "react-native-vector-icons/AntDesign";
import Icon2 from "react-native-vector-icons/Feather";
import styles from "./ContactScreen_Styles";
import { _rootInit } from "./utilities/Global";
import { BackgroundController } from "./../components/BackgroundController";
import {
  LoopAnim,
  ScaleAnim,
  EnterAnim,
  RotateAnim,
} from "./utilities/Animations";
const _root = _rootInit();

const ContactScreen = ({ navigation }) => {
  try {
    const route = useRoute();
    const [header, setHeader] = useState(
      _root.headers.headerContact.src + "?cache=" + Math.random()
    );
    const [enterRun, setEnterRun] = useState(false);
    useEffect(() => {
      const unsubscribe = navigation.addListener("focus", () => {
        analytics().logScreenView({
          screen_class: route.name,
          screen_name: route.name,
        });
        setEnterRun(true);
      });
      const unsubscribeBlur = navigation.addListener("blur", () => {
        setEnterRun("off");
      });
      return unsubscribe, unsubscribeBlur;
    }, []);

    return (
      <BackgroundController>
        <View style={styles.header}>
          <View style={styles.cover}>
            <Image
              style={styles.coverImage}
              source={{
                uri: header,
              }}
            />
            <View
              style={{
                width: 30,
                height: 30,
                position: "absolute",
                margin: 10,
                top: 0,
                left: 0,
              }}
            >
              <RotateAnim
                image={3}
                speed={6}
                reverse={false}
                run={enterRun}
                appear={12}
                appearReverse={true}
              />
            </View>
            <View
              style={{
                width: 50,
                height: 50,
                position: "absolute",
                margin: 30,
                top: "60%",
                left: 50,
              }}
            >
              <RotateAnim
                run={enterRun}
                image={0}
                speed={6}
                reverse={false}
                appear={3}
                appearReverse={true}
              />
            </View>
            <View
              style={{
                width: 60,
                height: 60,
                position: "absolute",
                margin: 10,
                top: "0%",
                left: "0%",
              }}
            >
              <RotateAnim
                run={enterRun}
                image={2}
                speed={10}
                reverse={true}
                appear={6}
              />
            </View>
            <View
              style={{
                width: 40,
                height: 40,
                position: "absolute",
                margin: 30,
                top: "72%",
                left: "20%",
              }}
            >
              <RotateAnim
                run={enterRun}
                image={1}
                speed={10}
                reverse={false}
                appear={5}
                appearReverse={true}
              />
            </View>
            <View
              style={{
                width: 40,
                height: 40,
                position: "absolute",
                margin: 30,
                top: "42%",
                left: "80%",
              }}
            >
              <RotateAnim
                run={enterRun}
                image={5}
                speed={12}
                reverse={true}
                appear={20}
              />
            </View>
            <View
              style={{
                width: 110,
                height: 110,
                position: "absolute",
                margin: 30,
                top: "62%",
                left: "40%",
              }}
            >
              <RotateAnim
                run={enterRun}
                image={4}
                speed={4}
                reverse={true}
                appear={14}
              />
            </View>
          </View>
        </View>
        <View style={styles.contactContainer}>
          <View style={styles.formField}>
            <Text style={styles.infoContact}>
              Somos un equipo de diseñadores{" "}
              <Text style={styles.bold}>UI, consultores UX y developers</Text>{" "}
              preocupados por transmitir el conocimiento que hemos adquirido a
              lo largo de varios proyectos, más de 10 años de experiencia sumada
              nos convierte en gurús del UX.
            </Text>
          </View>
          <View style={styles.formField}>
            <View style={styles.titleSpace}>
              <Text style={styles.formLabelTitle}>Siguenos en Instagram:</Text>
            </View>
            <Text
              style={styles.formLabel}
              onPress={() => {
                analytics().logShare({
                  content_type: "instagram",
                  item_id:"instagram",
                  method:"instagram"
                });
                Linking.openURL(
                  "https://www.instagram.com/uxapp.com.mx"
                ).catch((err) => console.error("Couldn't load page", err));
              }}
            >
              <Text style={styles.bold}>
                <Icon2 name="instagram" size={18} color={"#809EFF"}></Icon2>{" "}
                @uxapp.com.mx
              </Text>
            </Text>
          </View>
          <View style={styles.formField}>
            <View style={styles.titleSpace}>
              <Text style={styles.formLabelTitle}>
                ¿Estás en búsqueda de empleo?
              </Text>
            </View>
            <Text
              style={styles.formLabel}
              onPress={() => {
                analytics().logShare({
                  content_type: "instagram",
                  item_id:"instagram",
                  method:"instagram"
                });
                Linking.openURL(
                  "https://www.instagram.com/vacantesdigitales"
                ).catch((err) => console.error("Couldn't load page", err));
              }}
            >
              <Text style={styles.bold}>
                <Icon2 name="instagram" size={18} color={"#809EFF"}></Icon2>{" "}
                vacantesdigitales
              </Text>
            </Text>
            <Text
              style={styles.formLabel}
              onPress={() => {
                analytics().logShare({
                  content_type: "linkedin",
                  item_id:"linkedin",
                  method:"linkedin"
                });
                Linking.openURL(
                  "https://www.linkedin.com/company/vacantesdigitales"
                ).catch((err) => console.error("Couldn't load page", err));
              }}
            >
              <Text style={styles.bold}>
                <Icon2 name="linkedin" size={18} color={"#809EFF"}></Icon2>{" "}
                vacantesdigitales
              </Text>
            </Text>
            <Text
              style={styles.formLabel}
              onPress={() => {
                analytics().logShare({
                  content_type: "twitter",
                  item_id:"twitter",
                  method:"twitter"
                });
                Linking.openURL(
                  "https://twitter.com/vacantesdigital"
                ).catch((err) => console.error("Couldn't load page", err));
              }}
            >
              <Text style={styles.bold}>
                <Icon2 name="twitter" size={18} color={"#809EFF"}></Icon2>{" "}
                @vacantesdigital
              </Text>
            </Text>
          </View>

          <View style={styles.terms}>
            <Text
              style={styles.termsText}
              onPress={() => {
                analytics().logShare({
                  content_type: "privacy",
                  item_id:"privacy",
                  method:"privacy"
                });
                Linking.openURL("https://uxapp.com.mx/privacy").catch((err) =>
                  console.error("Couldn't load page", err)
                );
              }}
            >
              Aviso de privacidad |{" "}
            </Text>
            <Text
              style={styles.termsText}
              onPress={() => {
                analytics().logShare({
                  content_type: "terms",
                  item_id:"terms",
                  method:"terms"
                });
                Linking.openURL("https://uxapp.com.mx/terms").catch((err) =>
                  console.error("Couldn't load page", err)
                );
              }}
            >
              Términos y condiciones
            </Text>
          </View>
        </View>
      </BackgroundController>
    );
  } catch (e) {
    //crashlytics().crash();
  }
};

export default ContactScreen;
