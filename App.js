import { StyleSheet, Text, View, I18nManager } from "react-native";
import "react-native-gesture-handler";
import * as RNIap from "react-native-iap";
import * as React from "react";
import styles from "./App_Styles";
//import { nanoid } from "nanoid";

import crashlytics from "@react-native-firebase/crashlytics";
import { NavigationContainer } from "@react-navigation/native";
import Toast, { BaseToast } from "react-native-toast-message";
import {
  createStackNavigator,
  HeaderStyleInterpolators,
} from "@react-navigation/stack";
import { useRoute } from "@react-navigation/native";
import HomeScreen from "./pages/HomeScreen";
import DetailScreen from "./pages/DetailScreen";
import ProfileScreen from "./pages/ProfileScreen";
import FavoritesScreen from "./pages/FavoritesScreen";
import ContactScreen from "./pages/ContactScreen";
import SubscriptionScreen from "./pages/SubscriptionScreen";
import SettingsScreen from "./pages/SettingsScreen";
import {
  saveFavorite,
  getFavorites,
  initFavorites,
  viewIconFav,
  getNumFavorites,
} from "./pages/utilities/SaveFavorite";

import {
  setAsyncSubscription,
  getAsyncSubscription,
} from "./pages/utilities/SaveSubscription.js";

import FullDetailScreen from "./pages/FullDetailScreen";
import { ModalPortal } from "react-native-modals";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import Icon from "react-native-vector-icons/AntDesign";
import Icon2 from "react-native-vector-icons/Feather";
import { _rootInit } from "./pages/utilities/Global";
import {
  BackgroundController,
  updateBackground,
} from "./components/BackgroundController";
const _root = _rootInit();

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const toastConfig = {
  quote: ({ text1, text2, ...rest }) => {
    return (
      <View style={styles.toastCustomContainer}>
        <Text style={styles.toastCustomText}>“ {text1} ✨”</Text>
        <View style={styles.toastCustomAuthorContainer}>
          <Text style={styles.toastCustomAuthor}>- {text2}</Text>
        </View>
      </View>
    );
  },
  warning: ({ text1, text2, ...rest }) => (
    <BaseToast
      {...rest}
      style={{
        paddingBottom: 10,
        paddingTop: 10,
        height: "auto",
        borderLeftColor: "#FFC41B",
      }}
      text1Style={{
        fontSize: 16,
        fontWeight: "bold",
        paddingLeft: 10,
      }}
      text2Style={{
        fontSize: 16,
        fontWeight: "normal",
        paddingLeft: 10,
      }}
      text1={text1}
      text2={`${text2}`}
    />
  ),
  error: ({ text1, text2, ...rest }) => (
    <BaseToast
      {...rest}
      style={{
        paddingBottom: 10,
        paddingTop: 10,
        height: "auto",
        borderLeftColor: "red",
      }}
      text1Style={{
        fontSize: 16,
        fontWeight: "bold",
        paddingLeft: 10,
      }}
      text2Style={{
        fontSize: 16,
        fontWeight: "normal",
        paddingLeft: 10,
      }}
      text1={`${text1}`}
      text2={`${text2}`}
    />
  ),
  done: ({ text1, text2, ...rest }) => (
    <BaseToast
      {...rest}
      style={{
        paddingBottom: 10,
        paddingTop: 10,
        height: "auto",
        borderLeftColor: "#daf033",
      }}
      text1Style={{
        fontSize: 16,
        fontWeight: "bold",
        paddingLeft: 10,
      }}
      text2Style={{
        fontSize: 16,
        fontWeight: "normal",
        paddingLeft: 10,
      }}
      text1={`${text1}`}
      text2={`${text2}`}
    />
  ),
};

export default function App({ navigation }) {
  try {
    async function syncFavorites() {
      const enabledIconFavorite = await viewIconFav();
      if (enabledIconFavorite == "false") {
        let getAllFavorites = await getFavorites();
        const setInitFavorites =
          getAllFavorites.length > 0 ? getAllFavorites.length : null;
        // setNumFavs(setInitFavorites);
      }
    }

    async function getLocalAsyncSubscription() {
      //Obetenr la data de async storage.
      const showAsyncSubscription = await getAsyncSubscription();
      if (showAsyncSubscription.subscription == "active") {
        _root.entities.premium = true;
        _root.entities.subscriptionType = "active";
      } else if (showAsyncSubscription.subscription == "expired") {
        _root.entities.premium = false;
        _root.entities.subscriptionType = "expired";
      } else {
        _root.entities.premium = false;
        _root.entities.subscriptionType = "none";
      }
      // console.log("*****showAsyncSubscription", showAsyncSubscription);
    }
    React.useEffect(() => {
      crashlytics().log("App mounted.");
      getLocalAsyncSubscription();

      //Elimina transacciones no terminadas.
      RNIap.initConnection().then(async () => {
        // console.log("intentando limpiar");
        RNIap.clearTransactionIOS();
      });
    }, []);

    function InternalSectionsTabs() {
      const [numFavs, setNumFavs] = React.useState(null);
      React.useEffect(() => {
        initFavorites();
        syncFavorites();

        _root.watchFavs.get().subscribe(async (num) => {
          if (num) {
            const show = await viewIconFav();
        
            if (show == "false") {
            
              if (num == 0) {
                setNumFavs(null);
              } else {
                setNumFavs(num);
              }
            }
          } else {
            setNumFavs(null);
          }
        });
        //////////
        viewIconFav().then(async (result) => {
          if (result == "false") {
            const newNumFavorites = await getNumFavorites();
            _root.watchFavs.send(newNumFavorites);
          }
        });
      }, []);

      return (
        <Tab.Navigator
          lazy={false}
          tabBarOptions={{
            //activeTintColor: "tomato",
            //inactiveTintColor: "rgba(0,0,0,.4)",

            //activeTintColor:'blue',
            activeTintColor: "#809EFF",
            labelStyle: {
              fontSize: 12,
              //paddingBottom: 35
              marginBottom: 36,
            },
            tabStyle: {
              height: 90,
              backgroundColor: updateBackground(),
            },

            style: {
              height: 90,
              paddingTop: 8,
              marginBottom: 6,
              backgroundColor: updateBackground(),
              borderTopColor: "grey", //Change Like This
              borderTopWidth: 0,
              paddingLeft: 10,
              paddingRight: 10,

              shadowColor: "rgba(0,0,0,.9)",
              shadowOffset: {
                width: 0,
                height: 10,
              },
              shadowOpacity: 0.3,
              shadowRadius: 15,
              elevation: 13,
            },
            safeAreaInsets: "top",
          }}
          initialRouteName={"Mi guía UX"}
        >
          <Tab.Screen
            name="UX App"
            component={DetailsStack}
            screenOptions={{
              headerShown: false,
            }}
            options={{
              tabBarIcon: ({ color }) => {
                return <Icon2 name="pen-tool" size={30} color={color} />;
              },

              // tabBarBadge: 3,
            }}
          />
          <Tab.Screen
            name="Favoritos"
            component={FavoritesStack}
            screenOptions={{ headerShown: false }}
            options={{
              tabBarIcon: ({ color }) => {
                return <Icon2 name="heart" size={30} color={color} />;
              },

              tabBarBadge: numFavs,
            }}
            listeners={{
              focus: (e) => {
                setNumFavs(null);
                viewIconFav("true");
              },
              tabPress: async (e) => {},
            }}
          />
          {/* <Tab.Screen
            name="Meetings"
            component={FavoritesStack}
            screenOptions={{ headerShown: false }}
            options={{
              tabBarIcon: ({ color }) => {
                return <Icon2 name="share-2" size={30} color={color} />;
              },
            }}
          /> */}
          <Tab.Screen
            name="Mi perfil"
            component={ProfileScreen}
            screenOptions={{ headerShown: false }}
            options={{
              tabBarIcon: ({ color }) => {
                return <Icon2 name="user" size={30} color={color} />;
              },

              // tabBarBadge: 3,
            }}
            listeners={{
              tabPress: async (e) => {},
            }}
          />
          <Tab.Screen
            name="Opciones"
            component={SettingsStack}
            screenOptions={{ headerShown: false }}
            options={{
              tabBarIcon: ({ color }) => {
                return <Icon2 name="settings" size={30} color={color} />;
              },

              // tabBarBadge: 3,
            }}
            listeners={{
              tabPress: async (e) => {},
            }}
          />

          {/* <Tab.Screen
            name="Contáctanos"
            component={ContactScreen}
            screenOptions={{ headerShown: false }}
            options={{
              tabBarIcon: ({ color }) => {
                return <Icon2 name="coffee" size={30} color={color} />;
              },

              // tabBarBadge: 3,
            }}
            listeners={{
              tabPress: async (e) => {
                //const enabledIconFavorite = await viewIconFav();
              },
            }}
          /> */}
        </Tab.Navigator>
      );
    }

    function SettingsStack() {
      return (
        <Stack.Navigator
          screenOptions={{ headerShown: false }}
          initialRouteName={"SettingsScreen"}
          mode={"card"}
        >
          <Stack.Screen name="SettingsScreen" component={SettingsScreen} />
          <Stack.Screen
            name="Contacto"
            component={ContactScreen}
            options={{ headerShown: true, headerBackTitle: "Opciones" }}
          />
        </Stack.Navigator>
      );
    }

    function FavoritesStack() {
      return (
        <Stack.Navigator
          screenOptions={{ headerShown: false }}
          initialRouteName={"Favoritos"}
          mode={"card"}
        >
          <Stack.Screen name="Favoritos" component={FavoritesScreen} />
          <Stack.Screen
            name="FullDetailFavScreen"
            component={FullDetailScreen}
            options={{ headerShown: true, headerBackTitle: "volver" }}
          />
        </Stack.Navigator>
      );
    }
    function DetailsStack() {
      return (
        <Stack.Navigator
          screenOptions={{ headerShown: false }}
          initialRouteName={"DetailScreen"}
          mode={"card"}
        >
          <Stack.Screen name="DetailScreen" component={DetailScreen} />
          <Stack.Screen
            name="FullDetailScreen"
            component={FullDetailScreen}
            options={{ headerShown: true, headerBackTitle: "volver" }}
          />
        </Stack.Navigator>
      );
    }
    return (
      <NavigationContainer>
        <Stack.Navigator screenOptions={{ headerShown: false }} mode={"card"}>
          <Stack.Screen name="HomeScreen" component={HomeScreen} />
          <Stack.Screen
            name="Suscripción"
            component={SubscriptionScreen}
            options={{ headerShown: true, headerBackTitle: "Opciones" }}
          />
          <Stack.Screen
            name="InternalSectionsTabs"
            component={InternalSectionsTabs}
          />
        </Stack.Navigator>

        <ModalPortal />
        <Toast config={toastConfig} ref={(ref) => Toast.setRef(ref)} />
      </NavigationContainer>
    );
  } catch (e) {
    //crashlytics().crash();
    crashlytics().recordError(e);
  }
}
// https://reactnavigation.org/docs/hiding-tabbar-in-screens/
// https://github.com/ant-design/ant-design-icons/tree/master/packages/icons-react-native
// https://github.com/react-navigation/react-navigation/issues/1781
//ICONS https://oblador.github.io/react-native-vector-icons/
