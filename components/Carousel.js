import React, { Component, useState } from "react";
import {
  SafeAreaView,
  View,
  FlatList,
  StyleSheet,
  Text,
  StatusBar,
  SectionList,
  Switch,
  Image,
  ScrollView,
} from "react-native";
import Carousel, { Pagination } from "react-native-snap-carousel";


export default class MyCarousel extends Component {
    constructor(props) {
        super(props)
        this.state={
            entries: [
                {
                  title: "imagen1",
                  image:
                    "https://images.ctfassets.net/hrltx12pl8hq/4plHDVeTkWuFMihxQnzBSb/aea2f06d675c3d710d095306e377382f/shutterstock_554314555_copy.jpg",
                },
                {
                  title: "imagen2",
                  image:
                    "https://images.ctfassets.net/hrltx12pl8hq/4plHDVeTkWuFMihxQnzBSb/aea2f06d675c3d710d095306e377382f/shutterstock_554314555_copy.jpg",
                },
              ],
           
        }
    }

    _renderItem ({item, index}) {
        return     <View>
        <Text style={styles.title}>{item.title}</Text>
        <Image
          style={styles.imageTinder}
          source={{
            uri: item.image,
          }}
        />
      </View>
    }

    get pagination () {
        const { entries, activeSlide } = this.state;
        return (
            <Pagination
              dotsLength={entries.length}
              activeDotIndex={activeSlide}
              containerStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.75)' }}
              dotStyle={{
                  width: 10,
                  height: 10,
                  borderRadius: 5,
                  marginHorizontal: 8,
                  backgroundColor: 'rgba(255, 255, 255, 0.92)'
              }}
              inactiveDotStyle={{
                  // Define styles for inactive dots here
              }}
              inactiveDotOpacity={0.4}
              inactiveDotScale={0.6}
           
            />
        );
    }

    render () {
        return (
            <View>
                <Carousel
                  data={this.state.entries}
                  renderItem={this._renderItem}
                //   onSnapToItem={(index) => this.setState({ activeSlide: index }) }
                //   windowSize={1}
                />
                { this.pagination }
            </View>
        );
    }
}