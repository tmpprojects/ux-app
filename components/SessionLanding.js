import React, { useEffect, useState } from "react";
import {
  SafeAreaView,
  View,
  Text,
  ActivityIndicator,
  Image,
  Linking,
  Alert,
} from "react-native";
import {
    List,
    Switch,
    InputItem,
    Button,
    WhiteSpace,
    Checkbox,
    PickerView,
  } from "@ant-design/react-native";
  import { BackgroundController } from "./../components/BackgroundController";
  import registerNow from "./../design/welcome-subscription.png";
  import styles from "./../pages/ProfileScreen_Styles";
export function SessionLanding({ navigation }){
    return (
        <BackgroundController simple={true}>
          <View style={styles.center}>
            <Image source={registerNow} style={styles.registerNow}></Image>
            <View style={styles.centerTextContainer}>
              <Text style={styles.centerText}>
                Para poder ver esta sección es necesario tener iniciada una sesión
                en UX App.{" "}
              </Text>
              <View style={styles.centerButton}>
                <Button
                  onPress={() => {
                    navigation.navigate("HomeScreen", {
                      action: "suggestLogin",
                    });
                  }}
                >
                  Iniciar sesión
                </Button>
              </View>
            </View>
          </View>
        </BackgroundController>
      );
}