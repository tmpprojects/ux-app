import React, { useEffect, useState } from "react";
import { View, Text, Alert, ActivityIndicator } from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import auth from "@react-native-firebase/auth";
import crashlytics from "@react-native-firebase/crashlytics";
import analytics from "@react-native-firebase/analytics";
import { GoogleSignin } from "@react-native-community/google-signin";
import { appleAuth } from "@invertase/react-native-apple-authentication";
import firestore from "@react-native-firebase/firestore";
import styles from "./Login_Styles";
import "react-native-get-random-values";
import { v4 as uuid } from "uuid";
//import { List, Radio, WhiteSpace } from '@ant-design/react-native';ç
import Button from "@ant-design/react-native/lib/button";
import Icon from "react-native-vector-icons/AntDesign";
import DeviceInfo from "react-native-device-info";
import { validateRemoteReceipt } from "./../pages/utilities/ValidateReceipt";

//import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import moment from "moment";
import "moment-timezone";
moment().tz("America/Mexico_City").format();

import {
  GoogleSocialButton,
  AppleSocialButton,
} from "react-native-social-buttons";

import { _rootInit } from "./../pages/utilities/Global";
let _root = _rootInit();
GoogleSignin.configure();
///////////////////////////////////////PREVIOS HOME

const setUniqId = async () => {
  try {
    const uniqId = await AsyncStorage.getItem("uniqId");
    const setUniqId = uuid();
    if (uniqId !== null) {
      //console.log("Ya estaba añadido un uniqId ", uniqId);
      _root.user.uniqId = uniqId;
    } else {
      //console.log("Añadimos por primera vez el uniqId ", setUniqId);
      await AsyncStorage.setItem("uniqId", `${setUniqId}`);
      _root.user.uniqId = setUniqId;
    }

    crashlytics().setUserId(setUniqId);
  } catch (e) {
    //crashlytics().crash();
    crashlytics().recordError(e);
    console.log("Error al setear uniqId!", e.message);
  }
};
///////////////////////////////////////PREVIOS HOME

function emailIsValid(getEmail) {
  let email = getEmail.trim();
  return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);
}
function checkField(field) {
  return Boolean(field) ? field : null;
}
////////////////////////////////////////////

const Login = ({ navigation, hideGuestButton = false, loading = true }) => {
  let subscriber;
  const [initializing, setInitializing] = useState(true);
  const [validatingSession, setValidatingSession] = useState(true);
  const [textValidate, setTextValidate] = useState("Espera un momento...");
  const [automaticEnter, setAutomaticEnter] = useState(false);

  ////////////////////////////////////////////////ONCHANGE
  async function onAuthStateChanged(user) {
    if (user) {
      setAutomaticEnter(true);
      if (!automaticEnter) {
        console.log("HAY  UN USUARIO", user);
        const result = { user };
        const ifExist = await checkUser(result);
        console.log(ifExist);
        if (ifExist) {
          readUser(result);
        } else {
          createUser(result);
        }

       
      }
    } else {
      console.log("no HAY  UN USUARIO");
    }

    if (initializing) setInitializing(false);
    setValidatingSession(false);
  }

  useEffect(() => {
    if (!loading) {
      const subscribeFocus = navigation.addListener("focus", () => {
        setAutomaticEnter(false);
        // console.log("Login focus");
        setUniqId();
      });

      subscriber = auth().onAuthStateChanged(onAuthStateChanged);
      return subscribeFocus, subscriber;
    }
  }, [loading]);

  if (initializing) return null;

  ////////////////////////////////////////////////APPLE
  async function onAppleButtonPress() {
    //await logout();
    // Start the sign-in request
    const appleAuthRequestResponse = await appleAuth.performRequest({
      requestedOperation: appleAuth.Operation.LOGIN,
      requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME],
    });

    // Ensure Apple returned a user identityToken
    if (!appleAuthRequestResponse.identityToken) {
      throw "Apple Sign-In failed - no identify token returned";
    }

    // Create a Firebase credential from the response
    const { identityToken, nonce } = appleAuthRequestResponse;
    const appleCredential = auth.AppleAuthProvider.credential(
      identityToken,
      nonce
    );

    // Sign the user in with the credential
    return auth().signInWithCredential(appleCredential);
  }
  ////////////////////////////////////////////////GOOGLE
  async function onGoogleButtonPress() {
    //await logout();
    // Get the users ID token
    const { idToken } = await GoogleSignin.signIn();
    //console.log(idToken);

    // Create a Google credential with the token
    const googleCredential = auth.GoogleAuthProvider.credential(idToken);
    //console.log(googleCredential);

    // Sign-in the user with the credential
    return auth().signInWithCredential(googleCredential);
  }

  async function checkUser(result) {
    console.log("checkUser uid: ", result.user.uid);
    const uid = result.user.uid;
    const membersRef = firestore().collection("members");
    const snapshot = await membersRef.where("authId", "==", uid).get();
    if (!snapshot.empty) {
      //Si hay datos del usuario.
      return true;
    } else {
      return false;
      //No hay datos del usuario se necesita crear uno.
    }
  }
  async function readUser(result) {
    console.log("********** readUser uid: ", result.user.uid);
    const uid = result.user.uid;
    const membersRef = firestore().collection("members");
    const snapshot = await membersRef.where("authId", "==", uid).get();
    let infoUser = [];
    snapshot.forEach((doc) => {
      infoUser = {
        ...doc.data(),
        id: doc.id,
      };
    });
    console.log("Info obtenida del usuario", infoUser);

    _root.user = { ..._root.user, ...infoUser };

    console.info("  _root.user ", _root.user);

    // });

    //Actualizar las entities con un usuario logueado
    _root.entities.memberType = "LoggedIn";


    await validateRemoteReceipt(_root.user.id);
    enterHome();
  }

  async function createUser(result) {
    console.log("createUser uid: ", result.user.uid);
    const uid = result.user.uid;

    let brand = await DeviceInfo.getBrand();
    let buildNumber = await DeviceInfo.getBuildNumber();
    let deviceId = await DeviceInfo.getDeviceId();
    let systemVersion = await DeviceInfo.getSystemVersion();
    let hasNotch = await DeviceInfo.hasNotch();
    const uniqId = await AsyncStorage.getItem("uniqId");
    const providerId = result.user.providerData[0].providerId;
    const name = result.user.displayName;
    const email = result.user.email;

    firestore()
      .collection("members")
      .add({
        providerId,
        name: name,
        email: email,
        uniqId,
        subscriptionType: "none",
        date: moment().format(),
        authId: uid,
        infoDevice: {
          brand,
          buildNumber,
          deviceId,
          systemVersion,
          hasNotch,
        },
      })
      .then((internalResult) => {
        analytics().logEvent("saveFirebase", {
          name: name,
          providerId,
          subscriptionType: "none",
          date: moment().format(),
          authId: uid,
          infoDevice: {
            brand,
            buildNumber,
            deviceId,
            systemVersion,
            hasNotch,
          },
        });
        console.log("Después de añadir, ahora volver a leer.");
        readUser(result);
      })
      .catch((e) => {
        crashlytics().recordError(e);
        console.log(e);
      });
  }

  function enterHome() {
    if (!loading) {
      setAutomaticEnter(false);
      navigation.navigate("InternalSectionsTabs");
    }
  }
  if (loading === true || validatingSession === true || automaticEnter===true) {
    return (
      <React.Fragment>
        <View style={styles.separator}>
          <Text style={styles.textLoading}>{textValidate}</Text>
        </View>
        <View style={styles.separator}>
          <ActivityIndicator
            size={"large"}
            color={"#ffffff"}
          ></ActivityIndicator>
        </View>
      </React.Fragment>
    );
  }

  if (
    loading === false &&
    validatingSession === false && automaticEnter===false
    // && isRegisterUser == false
  ) {
    return (
      <View style={styles.buttonsContainer}>
        <View style={styles.separator}>
          <View style={styles.socialButton}>
            <AppleSocialButton
              onPress={() => {
                onAppleButtonPress().then(async (result) => {
                  const ifExist = await checkUser(result);
                  console.log(ifExist);
                  if (ifExist) {
                    readUser(result);
                  } else {
                    createUser(result);
                  }
                });
              }}
              textStyle={{
                fontSize: 16,
                color: "#636363",
              }}
              buttonText={"Iniciar sesión con Apple"}
            ></AppleSocialButton>
          </View>
        </View>
        <View style={styles.separator}>
          <View style={styles.socialButton}>
            <GoogleSocialButton
              onPress={() => {
                onGoogleButtonPress().then(async (result) => {
                  const ifExist = await checkUser(result);
                  if (ifExist) {
                    readUser(result);
                  } else {
                    createUser(result);
                  }
                });
              }}
              textStyle={{
                fontSize: 16,
                color: "#636363",
              }}
              buttonText={"Iniciar sesión con Google"}
            ></GoogleSocialButton>
          </View>
        </View>

        {!hideGuestButton && (
          <View style={styles.socialButton}>
            <Button
              style={{
                alignItems: "center",
                borderColor: "white",
              }}
              onPress={() => {
                analytics().logLogin({
                  method: "guest",
                });
                _root.entities.memberType = "Guest";
                _root.entities.subscriptionType = "none";
                enterHome();
              }}
            >
              <View
                style={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "center",
                  alignContent: "center",
                  alignItems: "center",
                }}
              >
                <Icon name="meh" size={24} color={"#636363"} />
                <View
                  style={{
                    marginLeft: 20,
                  }}
                >
                  <Text
                    style={{
                      color: "#636363",
                      fontSize: 16,
                    }}
                  >
                    Continuar como invitad@
                  </Text>
                </View>
              </View>
            </Button>
          </View>
        )}
      </View>
    );
  }
  return null;
};

export default Login;

//ICONS https://oblador.github.io/react-native-vector-icons/
