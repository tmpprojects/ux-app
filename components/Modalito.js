import React, { useState, useEffect, useRef } from "react";
import {
  Modal,
  ModalPortal,
  SlideAnimation,
  ModalContent,
  ModalTitle,
  ModalFooter,
  ModalButton,
} from "react-native-modals";
import {
  View,
  Text,
  SectionList,
  Image,
  Pressable,
  Platform,
  Button,
  Animated,
  Easing,
  Alert,
  ScrollView,
} from "react-native";
////////////////////////////3th////////////////////////////

import Icon from "react-native-vector-icons/AntDesign";

import {
  saveFavorite,
  deleteFavorite,
  getFavorites,
} from "../pages/utilities/SaveFavorite";

import moment from "moment";
import "moment-timezone";
moment().tz("America/Mexico_City").format();
//https://storage.googleapis.com/ux-tips-9e78f.appspot.com/post-1_1.png

import { _rootInit } from "../pages/utilities/Global";
const _root = _rootInit();

import styles from "../pages/DetailScreen_Styles";
import stylesInteraction from "../pages/utilities/Animation_Styles";
import lock from "./../design/lock.png";
import unlock from "./../design/unlock.png";
import members from "./../design/members.png";
import membersrRegister from "./../design/membersrRegister.png";
import logoPremium1x from "./../design/logo-premium-1x.png";

import { BackgroundController } from "./../components/BackgroundController";

function Modalito({ modalVisible, setModalVisible, navigation }) {
  function LoginPlease() {
    return (
      <React.Fragment>
        <View style={styles.loginPlease}>
          <View  style={styles.logoPremium1xContainer}><Image source={logoPremium1x} style={styles.logoPremium1x}></Image></View>
          
          <Text style={styles.membersText}>
            Este contenido es premium, necesitas tener una{" "}
            <Text style={styles.bold}>sesión iniciada en UX App</Text> .
          </Text>
        </View>
      </React.Fragment>
    );
  }

  return (
    <Modal
      width={300}
      visible={modalVisible}
      // onTouchOutside={() => {
      //   setModalVisible(false);
      // }}
      modalAnimation={
        new SlideAnimation({
          slideFrom: "bottom",
        })
      }
      // modalTitle={
      //   <ModalTitle
      //     title={
      //       <Text>
      //         <Icon name="lock" size={18} color={"rgba(0,0,0,.3)"} /> UX APP
      //         Premium ⚡️
      //       </Text>
      //     }
      //   />
      // }
      // swipeDirection={["up", "down", "left", "right"]}
      // onSwipeOut={(event) => {
      //   setModalVisible(false);
      // }}
      footer={
        <ModalFooter>
          <ModalButton
            text={"Cancelar"}
            onPress={() => {
              setModalVisible(false);
            }}
            textStyle={{
              color: "rgba(0,0,0,.5)",
            }}
          />
          <ModalButton
            text={"Iniciar sesión"}
            onPress={() => {
              setModalVisible(false);
              setTimeout(() => {
                navigation.navigate("HomeScreen", {
                  action: "suggestLogin",
                });
              }, 800);
            }}
            textStyle={{
              color: "#809EFF",
            }}
          />
        </ModalFooter>
      }
    >
      <ModalContent>
        <LoginPlease></LoginPlease>
      </ModalContent>
    </Modal>
  );
}

export default Modalito;
