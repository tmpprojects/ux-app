import React, { useState, useEffect, useRef } from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";
import Toast from "react-native-toast-message";

import {
  Modal,
  ModalPortal,
  SlideAnimation,
  ModalContent,
  ModalTitle,
  ModalFooter,
  ModalButton,
} from "react-native-modals";
import {
  View,
  Text,
  SectionList,
  Image,
  Pressable,
  Platform,
  Button,
  Animated,
  Easing,
  Alert,
  ScrollView,
} from "react-native";
////////////////////////////3th////////////////////////////
import analytics from "@react-native-firebase/analytics";
import crashlytics from "@react-native-firebase/crashlytics";
import { useRoute } from "@react-navigation/native";
import Icon2 from "react-native-vector-icons/Feather";
import Icon from "react-native-vector-icons/AntDesign";
import {
  saveFavorite,
  deleteFavorite,
  getFavorites,
  getNumFavorites
} from "./../pages/utilities/SaveFavorite";
import {
  ScaleAnim,
  EnterAnim,
  RotateAnim,
} from "./../pages/utilities/Animations";
import {
  Grayscale as PremiumFX,
  Tritanopia as PremiumMemberFX,
} from "react-native-color-matrix-image-filters";

import moment from "moment";
import "moment-timezone";
moment().tz("America/Mexico_City").format();
//https://storage.googleapis.com/ux-tips-9e78f.appspot.com/post-1_1.png

import { _rootInit } from "./../pages/utilities/Global";
const _root = _rootInit();

import styles from "../pages/DetailScreen_Styles";
import stylesInteraction from "./../pages/utilities/Animation_Styles";
import lock from "./../design/lock.png";
import unlock from "./../design/unlock.png";
import members from "./../design/members.png";
import membersrRegister from "./../design/membersrRegister.png";

import { BackgroundController } from "./../components/BackgroundController";

function Item({
  content,
  navigation,
  setModalVisible,
  member,
  goIntern,
  disabledFavorite = false,
  syncFavorites = () => {},
}) {
  const fadeAnim = useRef(new Animated.Value(0)).current;

  const [isFavorite, setIsFavorite] = useState(
    _root.favorites[content.id] ? true : false
  );
  const [run, setRun] = useState("off");
  const [isDelete, setIsDelete] = useState(false);

  useEffect(() => {
    let unsubscribeItem = navigation.addListener("focus", () => {
      // console.log("desde item");
      setRun("off");
      setIsFavorite(_root.favorites[content.id] ? true : false);
    });
    return () => {
      unsubscribeItem = null;
      setIsDelete(false);
    };
  }, []);

  function levelPost(level) {
    switch (level) {
      case "principiante":
        return styles.levelIcon1;
        break;
      case "intermedio":
        return styles.levelIcon2;
        break;
      case "avanzado":
        return styles.levelIcon3;
        break;
      case "gurú":
        return styles.levelIcon4;
        break;
      default:
        return styles.levelIcon1;
    }
  }

  function validateOpacity(content) {
    if (_root.entities.memberType == "Guest") {
      if (content.premium) {
        return styles.premiumOpacity;
      } else {
        return styles.normalOpacity;
      }
    } else {
      //Usuario registrado
      //Si es contenido premium pero tambien el usuario
      if (content.premium && _root.entities.premium == true) {
        return styles.normalOpacity;
      } else {
        if (content.premium) {
          return styles.premiumOpacity;
        } else {
          return styles.normalOpacity;
        }
      }
    }
  }

  function watchLock(debug = false) {
    let lockItem = true;

    if (_root.entities.memberType == "Guest") {
      //Es un visitante
      if (content.premium) {
        lockItem = true;
      } else {
        lockItem = false;
      }
    } else {
      //Es un usuario registrado
      //Es un visitante
      if (content.premium && member) {
        lockItem = false;
      } else {
        lockItem = true;
      }
    }
    if (debug) {
      console.log("_root.entities.memberType", _root.entities.memberType);
      console.log("content.premium", content.premium);
      console.log("member", member);
      console.log("lockItem", lockItem);
    }

    return lockItem;
  }

  function IconFavorite() {
    if (disabledFavorite) {
      return <Icon name="heart" size={26} style={styles.applyFavorite}></Icon>;
    } else {
      if (!isFavorite) {
        return <Icon name="hearto" size={26} style={styles.unFavorite}></Icon>;
      } else {
        return (
          <Icon name="heart" size={26} style={styles.applyFavorite}></Icon>
        );
      }
    }
  }

  if (isDelete) {
    // console.log("isDelete is true.", content.title);
    return null;
  }
  //console.log(content);
  return (
    <Pressable
      onPress={async () => {
        console.log(_root.entities, content.premium, member)
   
        if (!content.premium) {
          //////////////////////
          analytics().logEvent("clickItem", {
            id: content.id,
            title: content.title,
            premium: content.premium,
            userType: _root.entities.memberType,
            subscription: _root.entities.subscriptionType,
            open: "normal content",
          });
          ///////////////////////
          _root.selected = content;
          navigation.navigate(goIntern);
        } else {
          if (_root.entities.memberType == "Guest") {
            //////////////////////
            analytics().logEvent("clickItem", {
              id: content.id,
              title: content.title,
              premium: content.premium,
              userType: _root.entities.memberType,
              subscription: _root.entities.subscriptionType,
              open: "popup only guest",
            });
            ///////////////////////
            setModalVisible(true);
          } else {
            if (member) {
              //////////////////////
              analytics().logEvent("clickItem", {
                id: content.id,
                title: content.title,
                premium: content.premium,
                userType: _root.entities.memberType,
                subscription: _root.entities.subscriptionType,
                open: "content premium",
              });
              ///////////////////////
              _root.selected = content;
              navigation.navigate(goIntern);
            } else if (!member) {
              //////////////////////
              analytics().logEvent("clickItem", {
                id: content.id,
                title: content.title,
                premium: content.premium,
                userType: _root.entities.memberType,
                subscription: _root.entities.subscriptionType,
                open: "landing subscription",
              });
              ///////////////////////

              navigation.navigate("Suscripción");
            }
          }
        }
      }}
      style={styles.pressable}
    >
      <View style={styles.itemContainer}>
        <View style={styles.itemImage}>
          <View style={styles.normalOpacity}>
            {!content.premium && (
              <Image
                style={styles.itemImage}
                source={{
                  uri: content.image,
                }}
              />
            )}

            {watchLock() && content.premium && (
              <PremiumFX>
                <Image
                  style={styles.itemImageLock}
                  source={{
                    uri: content.image,
                  }}
                />
              </PremiumFX>
            )}

            {!watchLock() && content.premium && (
              <PremiumMemberFX>
                <Image
                  style={styles.itemImageLock}
                  source={{
                    uri: content.image,
                  }}
                />
              </PremiumMemberFX>
            )}
          </View>

          {(!content.premium || !watchLock()) && (
            <Pressable
              onPress={async () => {
                if (disabledFavorite) {
                  Alert.alert(
                    "Eliminar favorito",
                    "¿Estás segur@ de eliminar este favorito?",
                    [
                      {
                        text: "Sí",
                        onPress: async () => {
                          ///////////////////////
                          await deleteFavorite(content.id);
                          const newFavorites = await getFavorites();
                          const newNumFavorites = await getNumFavorites();
                          _root.watchFavs.send(newNumFavorites);
                          Toast.show({
                            type: "warning",
                            text1: content.title,
                            text2: "Eliminado de favoritos. 💔",
                            topOffset: 60,
                            visibilityTime: 1600,
                            autoHide: true,
                          });
                          //setIsDelete(true);
                          syncFavorites();
                          //////////////////////
                          analytics().logEvent("deleteFav", {
                            id: content.id,
                            title: content.title,
                            premium: content.premium,
                            userType: _root.entities.memberType,
                            subscription: _root.entities.subscriptionType,
                          });
                         
                          ///////////////////////
                          setRun("off");
                          setIsFavorite(false);
                          
                        },
                        style: "cancel",
                      },
                      {
                        cancelable: true,
                        text: "No",
                        style: "cancel",
                      },
                    ]
                  );
                } else {
                  if (isFavorite) {
                    await deleteFavorite(content.id);
                    setIsFavorite(false);
                    setRun("off");
                    const newFavorites = await getFavorites();
                    const newNumFavorites = await getNumFavorites();
                    _root.watchFavs.send(newNumFavorites);
           
                    Toast.show({
                      type: "warning",
                      text1: content.title,
                      text2: "Eliminado de favoritos. 💔",
                      topOffset: 60,
                      visibilityTime: 1600,
                      autoHide: true,
                    });
                    //////////////////////
                    analytics().logEvent("deleteFav", {
                      id: content.id,
                      title: content.title,
                      premium: content.premium,
                      userType: _root.entities.memberType,
                      subscription: _root.entities.subscriptionType,
                    });
                    ///////////////////////
                  } else {
                    Toast.show({
                      type: "done",
                      text1: content.title,
                      text2: "Añadido a favoritos. 💚",
                      topOffset: 60,
                      visibilityTime: 1600,
                      autoHide: true,
                    });
                    setRun(true);
                    await setIsFavorite(true);
                    await saveFavorite(content);
                 
                    const newFavorites = await getFavorites();
                    const newNumFavorites = await getNumFavorites();
                    
                    _root.watchFavs.send(newNumFavorites);
     
                    //////////////////////
                    analytics().logEvent("addFav", {
                      id: content.id,
                      title: content.title,
                      premium: content.premium,
                      userType: _root.entities.memberType,
                      subscription: _root.entities.subscriptionType,
                    });
                    ///////////////////////
                  }
                }
              }}
              style={styles.pressableItemFavorite}
            >
              <ScaleAnim run={run}>
                <IconFavorite></IconFavorite>
              </ScaleAnim>
            </Pressable>
          )}
          {watchLock() && content.premium && (
            <View style={styles.itemLockMessage}>
              <Image source={lock} style={styles.itemLockImage}></Image>
            </View>
          )}
          {!watchLock() && content.premium && (
            <View style={styles.itemUnLockMessage}>
              <Image source={unlock} style={styles.itemLockImage}></Image>
            </View>
          )}
        </View>

        <View style={styles.itemInfo}>
          {/* <Text style={styles.titleDebug}>{content.id}</Text>
            <Text style={styles.titleDebug}>{content.imageReal}</Text> */}

          {watchLock() && content.premium && (
            <Text style={styles.titleLock}>{content.title} </Text>
          )}
          {!content.premium && (
            <Text style={styles.title}>{content.title} </Text>
          )}
          {!watchLock() && content.premium && (
            <Text style={styles.title}>{content.title} </Text>
          )}

          {watchLock() && content.premium && (
            <Text style={styles.descriptionLock}>{content.subtitle}</Text>
          )}
          {!watchLock() && content.premium && (
            <Text style={styles.description}>{content.subtitle}</Text>
          )}
          {!content.premium && (
            <Text style={styles.description}>{content.subtitle}</Text>
          )}

          <View style={styles.moreDescriptionContainer}>
            <View style={levelPost(content.realLevel)}></View>
            <View style={styles.moreDescriptionText}>
              <Text style={styles.moreDescription}>{content.level}</Text>
            </View>
          </View>
        </View>
      </View>
    </Pressable>
  );
}

export default Item;
