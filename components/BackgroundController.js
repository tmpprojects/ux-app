import React, { useState, useEffect } from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";
import {
  SafeAreaView,
  View,
  FlatList,
  StyleSheet,
  Text,
  StatusBar,
  SectionList,
  Switch,
  Image,
  Pressable,
  Platform,
  Alert,
  ActivityIndicator,
  Linking,
  Button,
  Dimensions,
} from "react-native";
const windowWidth = Dimensions.get("window").width;
const windowHeight = Dimensions.get("window").height;
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

let selectedMode = "white";

export const black = "#282c34";
export const white = "#ffffff";

export function updateBackground() {
  if (selectedMode == "black") {
    return black;
  }
  return white;
}

export function updateFont() {
  if (selectedMode == "blank") {
    return black;
  }
  return white;
}

export function changeMode() {
  initMode();
  if (selectedMode === "black") {
    AsyncStorage.setItem("white");
  } else {
    AsyncStorage.setItem("black");
  }
}
export function BackgroundController({ children, noscroll, simple=false }) {
  // async function initMode() {
  //   const lookMode = await AsyncStorage.getItem("selectedMode");
  //   if (lookMode) {
  //     selectedMode = lookMode;
  //   } else {
  //     AsyncStorage.setItem("white");
  //   }
  // }
  // useEffect(async () => {
  //   initMode();
  // });

  if(simple){
    return children
  }
  return (
    <SafeAreaView
      style={{
        backgroundColor: white,
        marginTop: StatusBar.currentHeight || 0,
        minHeight: "100%",
      }}
    >
      {noscroll && children}
      {!noscroll && (
        <KeyboardAwareScrollView>{children}</KeyboardAwareScrollView>
      )}
    </SafeAreaView>
  );
}
