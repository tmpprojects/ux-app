import {
  SafeAreaView,
  View,
  FlatList,
  StyleSheet,
  Text,
  StatusBar,
  Button,
  ActivityIndicator,
  Modal,
  Pressable,
} from "react-native";
export default styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
    backgroundColor: "white",
    display: "flex",
    justifyContent: "flex-start",
  },
  textLoading:{
    color: "white",
  },
  item: {
    backgroundColor: "#f9c2ff",
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },

  backgroundVideo: {
    width: "100%",
    height: 300,
  },
  titleLogin: {
    fontSize: 18,
    marginBottom: 60,
    marginTop: 60,
    color: '#636363',
  },
  loginContainer: {
    display: "flex",
    alignItems: "center",
    alignContent: "center",
  },
  buttonsContainer:{
    display: "flex",
    alignItems: "center",
    alignContent: "center",
    justifyContent: "center",
  },
  separator:{
    marginBottom: 16,
  },
  loginButtonsContainer:{
    backgroundColor:'rgba(0,0,0,.06)',
    padding: 30,
    borderRadius: 20,
  },
  buttonsContainer:{
    display: "flex",
    width: '100%',
  },
  socialButton:{
    width:'100%',
  //  borderWidth: 3,
  //  borderColor: 'red',
    padding: 5,
    borderRadius: 18,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    // borderColor: "rgba(0,0,0,.4)",
    // borderWidth: 1,

    shadowColor: "rgba(0,0,0,.5)",
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: .6,
    shadowRadius: 8.3,
    //elevation: 13,
  },
  input:{
    width: 200,
    height:10,
    borderColor: 'red',
    borderWidth: 1,
  },
  welcomeText:{
    marginTop: 30,
    marginBottom: 20,
    color: "rgba(0,0,0,.8)"
  },

});
