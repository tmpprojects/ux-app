import React, { useState, useEffect } from "react";
import {
  SafeAreaView,
  View,
  FlatList,
  StyleSheet,
  Text,
  StatusBar,
  SectionList,
  Switch,
  Image,
  Pressable,
  Platform,
} from "react-native";

export default styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  toastCustomContainer: {
    height: "auto",
    width: "90%",
    backgroundColor: "white",
    padding: 16,
    borderRadius: 18,
    borderWidth: 1,
    borderColor: "rgba(0,0,0,.1)",

    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 4,

    elevation: 11,
  },
  toastCustomText: {
    fontSize: 16,
    color: "rgba(0,0,0,.6)",
  },
  toastCustomAuthor: {
    fontSize: 12,
    fontWeight: "normal",
    color: "rgba(0,0,0,.8)",
  },
  toastCustomAuthorContainer:{
      display: "flex",
      alignItems: "flex-end",
      marginTop: 10,
      
  }
});
