//////////////////////////////////////////////////////////
const type = "production";
//////////////////////////////////////////////////////////
function config() {
  if (type === "development") {
    return {
      password: "58c8d690278f4e8bac01b361abf7fc2c",
      cloudFunction:
        "http://localhost:5001/ux-tips-9e78f/us-central1/validateReceipt",
      syncCloudFunction:
        "https://us-central1-ux-tips-9e78f.cloudfunctions.net/syncSubscription",
    };
  } else {
    return {
      password: "58c8d690278f4e8bac01b361abf7fc2c",
      cloudFunction:
        "https://us-central1-ux-tips-9e78f.cloudfunctions.net/validateReceipt",
      syncCloudFunction:
        "https://us-central1-ux-tips-9e78f.cloudfunctions.net/syncSubscription",
    };
  }
}

export default config;
